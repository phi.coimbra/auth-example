import React, {useEffect, useState} from 'react'
import AsyncStorage from "@react-native-community/async-storage";

import io from 'socket.io-client';
import feathers from '@feathersjs/feathers';
import socketio from '@feathersjs/socketio-client';
import auth from '@feathersjs/authentication-client';
import _ from 'lodash';

import {
  createRxDatabase,
  addRxPlugin,
  removeRxDatabase
} from 'rxdb';

import {userSchema, eventoSchema, imageSchema, quizSchema} from './src/helpers/schema/Schema.js';

addRxPlugin(require('pouchdb-adapter-asyncstorage').default);
addRxPlugin(require('pouchdb-adapter-http'));

//const syncURL = 'http://antidoto:virus@192.168.1.48:5984/'; // Replace localhost with a public ip address!
//const syncURL = 'http://192.168.1.48:5050/'; 
const syncURL = 'http://talk.antido.to:5050/'; 
const dbName = 'antidoto';

//const API_URL = 'http://192.168.1.48:3030'
const API_URL = 'http://talk.antido.to:3030'

const socket = io(API_URL, {
  transports: ['websocket'],
  forceNew: true
});
export const clientAuth = feathers();

clientAuth.configure(socketio(socket));



clientAuth.configure(auth({
  storageKey: 'auth',
  storage:AsyncStorage
}))  

    
createDatabase = async () => {
  const db = await createRxDatabase({
      name: dbName,
      adapter: 'asyncstorage',
      password: 'antidoto1234',
      multiInstance: false,
  });

 

  console.log('create')

  await db.collection({
      name: 'users',
      schema:userSchema,
  });

  console.log('users')

  await db.collection({
    name: 'evento',
    schema:eventoSchema,
  });

  console.log('evento')

  await db.collection({
    name: 'images',
    schema:imageSchema,
  });

  console.log('images')

  await db.collection({
    name: 'quiz',
    schema:quizSchema,
   
  });

  
  console.log('quiz')

  console.log('schema')

  db.users.sync({
      remote: syncURL + dbName + '/users',
      options: {
          live: true,
          retry: true,
      },
  });

  const eventoSync = db.evento.sync({
    remote: syncURL + dbName + '/evento',
    options: {
        live: true,
        retry: true,
    },
  });

  db.images.sync({
    remote: syncURL + dbName,
    options: {
        live: true,
        retry: true,
    },
  });

  const quizSync = db.quiz.sync({
    remote: syncURL + dbName + '/quiz',
    options: {
        live: true,
        retry: true,
    },
  });

  //console.log('sync', meuevento)

  eventoSync.active$.subscribe(active => console.log('eventoSync?', active));
  //quizSync.active$.subscribe(active => console.log('quizSync?', active));
  /*imageSync.active$.subscribe(active => console.log('imageSync?', active));
  quizSync.active$.subscribe(active => console.log('quizSync?', active));

  eventoSync.alive$.subscribe(alive => console.log('eventoSyncAlive?', alive));
  quizSync.alive$.subscribe(alive => console.log('quizSyncAlive?', alive));
  
  quizSync.error$.subscribe(error => console.log('quizSyncError?',error));*/


  db.users.find().exec().then(doc =>  {
    console.log('resposta: ', doc, doc.length)
    if(doc){
      for(let i=0; i< doc.length; i++){
        console.log('resposta: ', doc[i].nome, doc[i].email)
      }
    }
  });
  //db.evento.findOne('42A8A87A-PLKT-9999-7777-0CJ15A709625').exec().then(doc => console.log('resposta: ', doc));

  /*const doc = await userCollection.insert({
    nome: 'Phillipe',
    sobrenome: 'Murray',
    email:'phi.coimbra@gmail.com'
  });*/
 //db.evento.find().exec().then(doc => console.log('resposta: ', doc));

 
 //userCollection.findOne('phi.coimbra@gmail.com').exec().then(doc => console.log('resposta: ', doc));

  console.log('find');
  //console.log('add: ', doc)


  //const doc = await db.evento.insert({nome:'qwerty', id:'42A8A87A-F71C-1112-B81D-0CJ15A709625'});

  //await db.remove();

  
  //console.log('inserted: ', db)

  return db;

  
}

//export const database = createDatabase();

export const useDB = () => {

  //console.log('INITDB')
  const [mydatabase, setDatabase] = useState(null);

  useEffect(async () => {

    const database = await createDatabase();
    setDatabase(database)

  },[]);

  return mydatabase;
}

//useDB()
  
  //removeRxDatabase('antidoto', AsyncStorage);
//AsyncStorage.clear()  