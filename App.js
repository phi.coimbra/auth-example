import React, {useState, useEffect, useReducer, useContext} from 'react';

import OneSignal from 'react-native-onesignal';
import _ from 'lodash';
import { View, Text, StyleSheet, Button, Platform, TouchableOpacity } from 'react-native';
import Login from './src/screens/login';
import Signup from './src/screens/signup';
import Perfiledit from './src/screens/perfiledit';
import DrawerContent from './src/screens/drawerMenu';
import Recover from './src/screens/recover';
import Detail from './src/detail';
import Home from './src/screens/home';
import Agenda from './src/screens/agenda';
import Chat from './src/screens/chat';
import Perfil from './src/screens/perfil';
import Quiz from './src/screens/quiz';
import Liveevent from './src/screens/liveevent';
import Splash from './src/screens/splash';

import Conteudos from './src/screens/conteudos';
import ContentPdf from './src/screens/contentPdf';
import Videoplayer from './src/screens/videoplayer';
import Livevideo from './src/screens/livevideo';
import Videos from './src/screens/videos';
import Tab3 from './src/screens/tabs/Tab3';

import { navigationRef } from './src/components/RootNavigation';
import * as RootNavigation from './src/components/RootNavigation.js';

import {
  NavigationContainer,
  DefaultTheme,
  DarkTheme,
  DrawerActions
} from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
//import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Appearance, useColorScheme, AppearanceProvider } from 'react-native-appearance';

import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon3 from 'react-native-vector-icons/MaterialIcons';

import { clientAuth, usersService, useDB } from './GunInit.js'
import { logginReducer, DB, Dispatch, getCliente, logout, reAuth, Auth, Users } from './src/helpers/hooks';

import useFetch from './src/helpers/hookDb';



const Drawer = createDrawerNavigator();
const LoginStack = createStackNavigator();
const Stack = createStackNavigator();
const RootStack = createStackNavigator();
const ScreensStack = createStackNavigator();
const MaterialBottomTabs = createMaterialBottomTabNavigator();
const MaterialTopTabs = createMaterialTopTabNavigator();

App = () => {

  const colorScheme = useColorScheme();
  const myDB = useDB()
  //const [logged] = useState(false)
  
  const reducer = useReducer(logginReducer, {logged:false, cliente:null})
  const [loading, setLoading] = useState(true)

  const [{logged, cliente}, dispatch] = reducer;

  useEffect(() => {

    if(myDB){
      //console.log('eaase: ', myDB)
      userLogged();
    }

    return () => {
     
    }
  }, [myDB])



  useEffect(() => {

    initOneSignal()

    setTimeout(() => {
      setLoading(false)
    }, 1000)

    return () => {
      OneSignal.removeEventListener('received', onReceived)
      OneSignal.removeEventListener('opened', onOpened)
      OneSignal.removeEventListener('ids', onIds)
    }
  }, [])

  const userLogged = async () => {
    let imLogged = await reAuth(clientAuth)
    let isCliente = await getCliente(myDB, imLogged.user.email)

    //console.log('is logged app: ', imLogged)

    if(isCliente){
      dispatch({type:'cliente', payload:isCliente})
      OneSignal.sendTags({cliente:isCliente, teste:true}) 
      //console.log('cliente: ', isCliente)
    }
    
    if(imLogged){
       // console.log('is logged app: ', imLogged.user)
        dispatch({type:'login'})
        dispatch({type:'userdata', payload:{email:imLogged.user.email}})
    }
  }

  function initOneSignal(){
    OneSignal.setLogLevel(6, 0);
  
    // Replace 'YOUR_ONESIGNAL_APP_ID' with your OneSignal App ID.
    OneSignal.init("45394fd0-0ad9-4438-be0e-70980c68d117", {kOSSettingsKeyAutoPrompt : false, kOSSettingsKeyInAppLaunchURL: false, kOSSettingsKeyInFocusDisplayOption:2});
    OneSignal.inFocusDisplaying(2); // Controls what should happen if a notification is received while the app is open. 2 means that the notification will go directly to the device's notification center.
    
    // The promptForPushNotifications function code will show the iOS push notification prompt. We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step below)
    OneSignal.promptForPushNotificationsWithUserResponse(myiOSPromptCallback);

    OneSignal.addEventListener('received', onReceived);
    OneSignal.addEventListener('opened', onOpened);
    OneSignal.addEventListener('ids', onIds);
  }

  function onReceived(notification) {
    console.log("Notification received: ", notification);
  }

  function onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData, _.keys(openResult.notification.payload.additionalData)[0]);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);


    if(_.keys(openResult.notification.payload.additionalData)[0] === 'live'){
      console.log('live');
      RootNavigation.navigate(openResult.notification.payload.additionalData.live, { screenName: openResult.notification.payload.additionalData.live })
    }
  }

  function onIds(device) {
    console.log('Device info: ', device);
  }

  function myiOSPromptCallback(permission){
    // do something with permission value
    console.log('PERMISSAO', permission)
  }


  if(loading){
    return <Splash />
  }

  const MyTheme = {
    dark: false,
    colors: {
      primary: 'white',
      background: '#ededed',
      card: '#0f0f0f',
      text: 'white',
      border: 'black',
    },
  }

  createHomeStack = () =>
  <DB.Provider value = {myDB}>
    <Dispatch.Provider value = {reducer}>
      <Auth.Provider value = {clientAuth}>
        <Users.Provider value = {usersService}>
        
          <Stack.Navigator
          headerMode="none"
          >  
          {!logged? 
            <Stack.Screen
              name="Login"
              children={this.LoginStackScreen}
              options={{headerShown: false}}
            />
            :
            <Stack.Screen
              name="Home"
              children={this.RootStackScreen}
              />
              
          }
            <Stack.Screen
              name="Screens"
              children={this.ScreensStackScreen}
              />  
            
          </Stack.Navigator>
        </Users.Provider>
      </Auth.Provider>
    </Dispatch.Provider>
  </DB.Provider>

LoginStackScreen = () => {
  return (
    <LoginStack.Navigator
      
      >  

      <ScreensStack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <ScreensStack.Screen
        name="Signup"
        component={Signup}
        options={{
          title: ""
        }}
      />
      <ScreensStack.Screen
        name="Recover"
        component={Recover}
        options={{
          title: "Recover"
        }}
      />
      <ScreensStack.Screen
        name="Perfiledit"
        component={Perfiledit}
        options={{
          title: "Perfil",
          header: ({ scene, previous, navigation }) => {
            const { options } = scene.descriptor;
            const title =
              options.headerTitle !== undefined
                ? options.headerTitle
                : options.title !== undefined
                ? options.title
                : scene.route.name;
          
            return (
              <View
                title={title}
                leftButton={
                  undefined
                }
                style={options.headerStyle}
              />
            );
          }
        }}
      />
     
  </LoginStack.Navigator>
)
}


 RootStackScreen = () => {
  return (
    <RootStack.Navigator
    //headerMode='screen'
    
    initialRouteName="Home"
        screenOptions={{
          
          gestureEnabled: true,
          cardOverlayEnabled: true,
          ...TransitionPresets.ModalPresentationIOS,
        }}
        mode="modal"
        
        >  
        <RootStack.Screen
        name="Home"
        children={this.createDrawer}
        options={({navigation, route}) => {
          //const { toggleDrawer } = props.navigation
          //console.log('props', route)
          return {
            //title: /*props.route.params.name*/'ok',
            headerShown:( _.get(navigation.dangerouslyGetState().routes[0], 'state.index') >= 1 ?
          
            false
            :
            true
            ),

          headerLeft: () => (
            <View style={{flexDirection: "row",justifyContent: "flex-end",paddingLeft:10}}>
              {_.get(navigation.dangerouslyGetState().routes[0], 'state.index') >= 1 /*_.has(navigation.dangerouslyGetState().routes[0].state, 'index')*/?
            <Icon.Button
                size={25}
                name="angle-left"
                backgroundColor="transparent"
                underlayColor="transparent" // This one
                color="white"
                onPress={() => navigation.navigate('Homes')}
              ></Icon.Button>
              :
              <Icon.Button
                size={25}
                name="bars"
                backgroundColor="transparent"
                underlayColor="transparent" // This one
                color="white"
                onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())}
              ></Icon.Button>
          }
          </View>
          ),
          headerRight: () => (
            
            <View style={{flexDirection: "row", justifyContent: "flex-end", width:100 }}>
              <Icon.Button
                size={24}
                name="bell"
                backgroundColor="transparent"
                underlayColor="transparent" // This one
                color="white"
                
              ></Icon.Button>
             {/* <Icon.Button
                size={25}
                name="user"
                backgroundColor="transparent"
                underlayColor="transparent" // This one
                color="white"
                onPress={() => navigation.navigate('Perfil')}
              ></Icon.Button>
              <Icon.Button
                size={25}
                name="cog"
                backgroundColor="transparent"
                underlayColor="transparent" // This one
                color="white"
                onPress={this.console.log}
              ></Icon.Button>
             */}
              
            </View>
          ),
          title: ( _.get(navigation.dangerouslyGetState().routes[0], 'state.index') >= 1 ?
          
          route.state.routeNames[route.state.index]
          :
          route.name
          )
        }}}
      />
      <RootStack.Screen
        name="Perfil"
        component={Perfiledit}
        />
        <RootStack.Screen
        name="Quiz"
        component={Quiz}
        options={({navigation, route}) => {
          //const { toggleDrawer } = props.navigation
          console.log('props', route)
          return {
            //title: /*props.route.params.name*/'ok',
            headerShown:false
          }
        }}
        />
       </RootStack.Navigator>
       
  )
      }

      ScreensStackScreen = () => {
        return (
          <ScreensStack.Navigator
          headerMode={'none'}
          >  

            <ScreensStack.Screen
              name="Detail"
              component={Detail}
              options={{
                title: "Detail Screen"
              }}
            />
            <ScreensStack.Screen
              name="Agenda"
              component={Agenda}
              options={{
                title: "Agenda"
              }}
            />
           
            
           
           
          </ScreensStack.Navigator>
        )
      }

  createDrawer = (props) =>
    <Drawer.Navigator drawerContent={props => <DrawerContent {...props} />}>
      <Drawer.Screen name="Homes" children={this.createBottomTabs} />
      <Drawer.Screen name="Agenda" options={{ drawerLabel: 'Updates', headerTitle:'ok', headerShown:false }} component={Agenda} />
     
      <Drawer.Screen name="Conteudo" component={Conteudos} />
      <Drawer.Screen name="Videos" component={Videos} />
      <Drawer.Screen name="Livevideo" component={Livevideo} />
      <Drawer.Screen name="Pdf" component={ContentPdf} />
      <Drawer.Screen name="Videoplayer" component={Videoplayer} />
      <Drawer.Screen name="Liveevent" component={Liveevent} />
    </Drawer.Navigator>

  createTopTabs = (props) => {
    return <MaterialTopTabs.Navigator>
      <MaterialTopTabs.Screen
        name="Videos"
        component={Conteudos}
        //options={{ title: props.route.params.name }}
      />
      <MaterialTopTabs.Screen name="Perguntas" component={Videos} />
    </MaterialTopTabs.Navigator>
  }

  createBottomTabs = (props) => {
    return <MaterialBottomTabs.Navigator
    shifting={false}>
      <MaterialBottomTabs.Screen
        name="Tab 1"
        
        component={Home}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: () => (
            <Icon size={25} name='home' /> 
          ),
        }}
      />
      <MaterialBottomTabs.Screen 
        name="Tab 2" 
        style={{ marginBottom: 16}}
        component={this.createTopTabs}
        listeners={{
          tabPress: e => {
            // Prevent default action
            props.navigation.navigate('Conteudo'); 
            e.preventDefault();
          },
        }}
        options={{
          
          tabBarLabel: 'Conteudo',
          tabBarIcon: () => (
            <Icon2 name="content-paste" size={25}  />
          )
          
        }}
      />
      
      <MaterialBottomTabs.Screen name="Tab 3" component={Livevideo}
      listeners={{
        tabPress: e => {
          // Prevent default action
          props.navigation.navigate('Videos'); 
          e.preventDefault();
        },
      }}
        options={{
          tabBarLabel: 'Videos',
          tabBarIcon: () => (
            <Icon2 size={25} name={'youtube-tv'} />
          ),
        }}
      />
      <MaterialBottomTabs.Screen name="Tab 4" component={Livevideo}
      listeners={{
        tabPress: e => {
          // Prevent default action
          props.navigation.navigate('Liveevent'); 
          e.preventDefault();
        },
      }}
        options={{
          tabBarLabel: 'Live',
          tabBarIcon: () => (
            <Icon2 size={25} name={'youtube-tv'} />
          ),
        }}
      />
      <MaterialBottomTabs.Screen name="Tab 5" component={Livevideo}
      listeners={{
        tabPress: e => {
          // Prevent default action
          props.navigation.navigate('Quiz'); 
          e.preventDefault();
        },
      }}
        options={{
          tabBarLabel: 'Quiz',
          tabBarIcon: () => (
            <Icon3 size={25} name={'perm-device-information'} />
          ),
        }}
      />
    </MaterialBottomTabs.Navigator>
  }

  

  return (
    <AppearanceProvider>
      <NavigationContainer ref={navigationRef} theme={/*colorScheme == 'dark' ? DarkTheme : */MyTheme}>
        {this.createHomeStack()}
      </NavigationContainer>
    </AppearanceProvider>
  );
}

export default App; 

// import React from 'react';
// import { Text, View } from 'react-native';

// const HelloWorldApp = () => {

//   const myDB = useDB()

//   return (
//     <View
//       style={{
//         flex: 1,
//         justifyContent: "center",
//         alignItems: "center"
//       }}>
//       <Text>Hello, world!</Text>
//     </View>
//   )
// }
// export default HelloWorldApp;