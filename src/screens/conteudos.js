import React, {useState, useEffect} from 'react';
import { View, Text, StyleSheet, Dimensions, ScrollView, TouchableOpacity, Image } from 'react-native';
import { useNavigation, useRoute, useIsFocused, useFocusEffect } from '@react-navigation/native';
import CatalogoItem from "../components/CatalogoItem";
import Video from 'react-native-video';

import { useSafeArea, SafeAreaView } from 'react-native-safe-area-context';
import MaterialButtonIn from "../components/MaterialButtonIn";
import Icon from 'react-native-vector-icons/Ionicons';
import { pdf64 } from "../assets/pdf/pdf64"

//import { styles } from '../styles/styles';

Conteudos = ({ navigation }) => {

  
  const [off, setOff] = useState(true)
  const insets = useSafeArea();

  useEffect(() => {
    // Atualiza o título do documento utilizando a API do navegador
    //console.log(myvideo)
  });


  return (
    <View style={[styles.container, {
      paddingTop: insets.top,
      //paddingBottom: insets.bottom
    }]}>
    <View style={{height:40}}>
            <TouchableOpacity 
                onPress={() => navigation.goBack()}
                style={{width:Dimensions.get('window').width-20, flex:1, flexDirection:'row'}}
            >
                <Icon 
                    name="ios-arrow-back" 
                    color={'#000'}
                    size={25}
                />
                <Text style={{fontSize:20, marginLeft:10}}>Voltar</Text>
            </TouchableOpacity>
        </View>
    <ScrollView 
    >
      <CatalogoItem navigation={navigation} imagem={{nome:require('../assets/images/pdf_talk.png')}} botaotext={'visualizar'} titulo={'Apresentação Talk'} page={'Pdf'} content={pdf64} type={'pdf'}/>
      
  </ScrollView>
  </View>
    
  );
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin:20
  },
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,

  },
  chat:{
    height:'65%'
  },
  video:{
    height:'35%'
  },
  image: {
    width: 400,
    height: 200,
    marginLeft: 9
  },
  bemVindoPhillipe2: {
    top: 220,
    left: 10,
    color: "#000",
    position: "absolute",
    fontSize: 18,
    fontFamily: "Montserrat-Regular",
    lineHeight: 17
  },
  materialButtonIn:{


    alignItems: 'stretch',

    marginTop:40,

    height: 56,
    marginRight:10,
    marginLeft:10
  },
})

export default Conteudos;