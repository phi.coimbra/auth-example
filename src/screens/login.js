import React, { useContext, useReducer, useEffect } from 'react';
import { View, KeyboardAvoidingView,  Text, StyleSheet, Button, Platform, TouchableOpacity, Image } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';

import Loginform from "../components/Loginform";
import MaterialHelperTextBox1 from "../components/MaterialHelperTextBox1";

import MaterialButtonViolet from "../components/MaterialButtonViolet";
import MaterialButtonWithShadow from "../components/MaterialButtonWithShadow";



import UUIDGenerator from 'react-native-uuid-generator';

//import { Gun } from '../../UserContext';
//import {DB} from '../helpers/hooks';

//import { styles } from './styles/styles';

//AsyncStorage.clear();

 UUIDGenerator.getRandomUUID((uuid) => {
  //console.log(uuid);
});

Login = () => {

  
  const navigation = useNavigation();
  const route = useRoute();
  //const db = useContext(DB)

  

  let detailResult = route.params;
  return (
   
    <React.Fragment>
      <KeyboardAvoidingView
      behavior={Platform.OS == "ios" ? "padding" : "height"}
      style={styles.container}
    >
        <View style={styles.topScreen}>
            <View style={styles.boxText}>
              <Text style={styles.textHeader}>Acesse sua conta</Text>
              <TouchableOpacity onPress={() => navigation.navigate('Signup', { screenName: "Signup" })}>
                <Text style={styles.textSubHeader}>Não tem conta? <Text style={{color: '#ffcc30'}}>crie uma aqui.</Text></Text>
              </TouchableOpacity>
            </View>
        </View>
    
        <View style={styles.items}>
          <View style={styles.login}>
            <Loginform />
            
          </View>
        </View> 
        </KeyboardAvoidingView>
    </React.Fragment>
   
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    
  },
  topScreen: {
    
    backgroundColor: "#000",
    justifyContent:'flex-start',
    height:90
  },
  items:{
    flex: 4,
    //flexDirection: "column",
    //marginTop:200,
    
    //alignItems: 'center',
  },
  login: {
    flex:1,
    justifyContent: 'space-around',
    //height: 330,
    alignItems: 'center',
    margin:30
  },

  infoimage: {
    flex:1,
    height: undefined, 
    width: undefined,
    alignSelf: 'stretch',
    maxHeight:50,
    alignContent:'center'
    //width: 400,
    //height: 400,
    //marginLeft: 9
  },
  
  
  boxText: {
    width: '100%',
    marginLeft:30,
    //height: 100
  },
  textHeader: {
    marginTop:15,
    height:35,
    color: "rgba(255,255,255,1)",
    fontSize: 30,
    fontFamily: "Montserrat-Regular"
  },
  textSubHeader: {
    height:20,
    color: "rgba(255,255,255,1)",
    fontSize: 15,
    fontFamily: "Montserrat-Regular",
    
  },
});

export default Login;