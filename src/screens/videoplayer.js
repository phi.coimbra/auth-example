import React, {useState, useEffect} from 'react';
import { View, Text, StyleSheet, Button, ScrollView, TouchableOpacity, Dimensions } from 'react-native';
import { useNavigation, useRoute, useIsFocused, useFocusEffect } from '@react-navigation/native';
import Catalogo from "../components/Catalogo";
import Video from 'react-native-video';

import YoutubePlayer from "react-native-yt-player";
import { useSafeArea, SafeAreaView } from 'react-native-safe-area-context';
import {  NodePlayerView } from 'react-native-nodemediaclient';
import Icon from 'react-native-vector-icons/Ionicons';


//import { styles } from '../styles/styles';

VideoPlayer = ({ route, navigation }) => {

  
  const [myvideo, setMyvideo] = useState()
  const [paused, setPause] = useState(true)
  const [off, setOff] = useState(false)
  const { content } = route.params;
  const [isfullscreen, setIsFullscreen] = useState(false)
  const insets = useSafeArea();

  const isFocused = useIsFocused();

  useFocusEffect(
    React.useCallback(() => {
        setOff(true)
      return () => {
        setOff(false)
      };
    }, []),
  );


  useEffect(() => {
   
    if(myvideo){
      console.log(myvideo.props.paused)
      //setPause(true)
      
      //!isFocused? myvideo.volume = 0: myvideo.volume = 1
    }
    
    
    // Atualiza o título do documento utilizando a API do navegador
    //console.log(myvideo)
  });

  const TopBar = ({ play, fullScreen }) => (
    <View
      style={{
        alignSelf: "center",
        position: "absolute",
        top: 0
      }}
    >
      <Text style={{ color: "#FFF" }}> Custom Top bar</Text>
    </View>
  )

  onFullScreen = (e) => {
    setIsFullscreen(e)
  }


  return (
    <View style={[styles.container, {
      paddingTop: insets.top,
      //paddingBottom: insets.bottom
    }]}>
        { !isfullscreen && (<TouchableOpacity 
            onPress={() => navigation.goBack()}
            style={{width:Dimensions.get('window').width-20, flexDirection:'row', margin:10}}
        >
            <Icon 
                name="ios-arrow-back" 
                color={'#000'}
                size={25}
            />
            <Text style={{fontSize:20, marginLeft:10}}>Voltar</Text>
        </TouchableOpacity>
        )}
        
        <View style={styles.video}>
        {off && (
        <YoutubePlayer
          isLive={content.live}
          loop
          topBar={TopBar}
          videoId={content.video}
          autoPlay
          onFullScreen={onFullScreen}
          onStart={() => console.log("onStart")}
          onEnd={() => alert("on End")}
        />
        )} 
        </View>
  </View>
    
  );
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "rgba(248,242,255,1)"
  },
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,

  },
  chat:{
    height:'65%'
  },
  video:{
    height:'35%'
  }
})

export default VideoPlayer;