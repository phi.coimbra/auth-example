import React, {Component, useState, useEffect} from 'react';
import { useSafeArea, SafeAreaView } from 'react-native-safe-area-context';
import { View, Text, StyleSheet, Button, ScrollView, TouchableOpacity, Dimensions } from 'react-native';
import {LocaleConfig, Calendar, CalendarList, Agenda} from 'react-native-calendars';
import Icon from 'react-native-vector-icons/Ionicons';

import { styles as styles2 } from '../styles/styles';

LocaleConfig.locales['ptBr'] = {
  monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
  monthNamesShort: ['Jan.','Fev.','Mar.','Abr.','Mai.','Jun.','Jul.','Ago.','Set.','Out.','Nov.','Dez.'],
  dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sabado'],
  dayNamesShort: ['Dom.','Seg.','Ter.','Qua.','Qui.','Sex.','Sab.'],
  today: 'Hoje'
};

LocaleConfig.defaultLocale = 'ptBr';

//const navigation = useNavigation();
  //const route = useRoute();

  //let detailResult = route.params;

AgendaScreen = ({navigation}) =>  {
  
    const insets = useSafeArea();
    const [items, setItems] = useState({})
    
  useEffect(() => {
    //console.log('ok', items)
    //loadItems('2017-05-16')
  },[])

  
  function loadItems(day){
    console.log('GET DATA !!!!')
    setTimeout(() => {
      for (let i = -15; i < 85; i++) {
        const time = day.timestamp + i * 24 * 60 * 60 * 1000;
        const strTime = timeToString(time);
        if (!items[strTime]) {
          items[strTime] = [];
          const numItems = Math.floor(Math.random() * 3 + 1);
          for (let j = 0; j < numItems; j++) {
            items[strTime].push({
              name: 'Item for ' + strTime + ' #' + j,
              height: Math.max(50, Math.floor(Math.random() * 150))
            });
          }
        }
      }
      const newItems = {};
      Object.keys(items).forEach(key => {newItems[key] = items[key];});
      
      setItems(newItems)
        
    }, 1000);
  }

  function renderItem(item){
    return (
      <TouchableOpacity
        //testID={testIDs.agenda.ITEM}
        style={[styles.item, {height: item.height}]}  
        onPress={() => console.log(item.name)}
      >
        <Text>{item.name}</Text>
      </TouchableOpacity>
    );
  }
  
  function renderEmptyDate(){
    return (
      <View style={styles.emptyDate}>
        <Text>This is empty date!</Text>
      </View>
    );
  }
  
  function rowHasChanged(r1, r2){
    return r1.name !== r2.name;
  }
  
  function timeToString (time){
    const date = new Date(time); 
    return date.toISOString().split('T')[0];
  }
  
  

 
  return (
    
    <View style={[styles2.container, {
      paddingTop: insets.top,
      //paddingBottom: insets.bottom,
      flex: 1, backgroundColor: 'white'}]}>

        <TouchableOpacity 
            onPress={() => navigation.goBack()}
            style={{width:Dimensions.get('window').width-20, flexDirection:'row', margin:10}}
        >
            <Icon 
                name="ios-arrow-back" 
                color={'#000'}
                size={25}
            />
            <Text style={{fontSize:20, marginLeft:10}}>Voltar</Text>
        </TouchableOpacity>
        
       
          <Agenda
                  //testID={testIDs.agenda.CONTAINER}
                  items={items}
                  loadItemsForMonth={loadItems}
                  selected={'2017-05-16'}
                  renderItem={renderItem}
                  renderEmptyDate={renderEmptyDate}
                  rowHasChanged={rowHasChanged}
                  // markingType={'period'}
                  // markedDates={{
                  //    '2017-05-08': {textColor: '#43515c'},
                  //    '2017-05-09': {textColor: '#43515c'},
                  //    '2017-05-14': {startingDay: true, endingDay: true, color: 'blue'},
                  //    '2017-05-21': {startingDay: true, color: 'blue'},
                  //    '2017-05-22': {endingDay: true, color: 'gray'},
                  //    '2017-05-24': {startingDay: true, color: 'gray'},
                  //    '2017-05-25': {color: 'gray'},
                  //    '2017-05-26': {endingDay: true, color: 'gray'}}}
                  // monthFormat={'yyyy'}
                  // theme={{calendarBackground: 'red', agendaKnobColor: 'green'}}
                  //renderDay={(day, item) => (<Text>{day ? day.day: 'item'}</Text>)}
                  // hideExtraDays={false}
                  theme={{
                    backgroundColor: '#efefef',
                    calendarBackground: '#ffffff',
                    textSectionTitleColor: '#b6c1cd',
                    selectedDayBackgroundColor: 'orange',
                    selectedDayTextColor: '#ffffff',
                    todayTextColor: '#00adf5',
                    dayTextColor: '#2d4150',
                    textDisabledColor: '#d9e1e8',
                    dotColor: '#00adf5',
                    selectedDotColor: '#ffffff',
                    arrowColor: 'orange',
                    disabledArrowColor: '#d9e1e8',
                    monthTextColor: 'blue',
                    indicatorColor: 'blue',
                    textDayFontFamily: 'Montserrat-Regular',
                    textMonthFontFamily: 'Montserrat-Regular',
                    textDayHeaderFontFamily: 'Montserrat-Regular',
                    textDayFontWeight: '300',
                    textMonthFontWeight: 'bold',
                    textDayHeaderFontWeight: '300',
                    textDayFontSize: 16,
                    textMonthFontSize: 16,
                    textDayHeaderFontSize: 14
                  }}
                />

       
  </View>

  );
}



const styles = StyleSheet.create({
  item: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17
  },
  emptyDate: {
    height: 15,
    flex:1,
    paddingTop: 30
  }
});

export default AgendaScreen;