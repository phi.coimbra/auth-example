import React, { useContext, useReducer, useEffect } from 'react';
import { View, Text, StyleSheet, KeyboardAvoidingView, Button, Platform, TouchableOpacity, Image } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';

import Signupform from "../components/Signupform";

Signup = () => {

  return (
    
    <React.Fragment>
      <KeyboardAvoidingView
      behavior={Platform.OS == "ios" ? "padding" : "height"}
      style={styles.container}
    >
      
        <View style={styles.topScreen}>
          <View style={styles.boxText}>
            <Text style={styles.textHeader}>Crie sua conta</Text>
          </View>
        </View>
    
        <View style={styles.items}>
          <View style={styles.login}>
            <Signupform />
          </View>
        </View> 
        </KeyboardAvoidingView>
    </React.Fragment>
    
   
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  topScreen: {
    flex:1,
    backgroundColor: "#000",
    justifyContent:'flex-end'
  },
  items:{
    flex: 5,
    //flexDirection: "column",
    //marginTop:200,
    //alignItems: 'center',
  },
  login: {
    flex:1,
    justifyContent: 'space-around',
    alignItems: 'center',
    margin:30
  },
  livelo: {
    width: 100,
    height: 100,
  },
  boxText: {
    width: '100%',
    marginLeft:30,
    marginBottom:20
  },
  textHeader: {
    height:35,
    color: "rgba(255,255,255,1)",
    fontSize: 30,
    fontFamily: "Montserrat-Regular"
  },
  textSubHeader: {
    height:40,
    color: "rgba(255,255,255,1)",
    fontSize: 15,
    fontFamily: "Montserrat-Regular",
  },
});

export default Signup;