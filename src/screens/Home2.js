import React, { Component } from "react";
import { StyleSheet, View, ScrollView, Text, Image } from "react-native";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import Svg, { Ellipse } from "react-native-svg";
import MaterialHeader4 from "../components/MaterialHeader4";
import Btnmenu from "../components/Btnmenu";
import IoniconsIcon from "react-native-vector-icons/Ionicons";

function Home(props) {
  return (
    <View style={styles.container}>
      <View style={styles.scrollArea}>
        <ScrollView
          horizontal={false}
          contentContainerStyle={styles.scrollArea_contentContainerStyle}
        >
          <View style={styles.loremIpsumColumn}>
            <Text style={styles.loremIpsum}>Usuários na plataforma</Text>
            <View style={styles.group}>
              <View style={styles.imageRow}>
                <Image
                  source={require("../assets/images/kisspng-computer-icons-avatar-social-media-blog-font-aweso-avatar-icon-5b2e99c4409623.4643918115297806762646.png")}
                  resizeMode="contain"
                  style={styles.image}
                ></Image>
                <Image
                  source={require("../assets/images/kisspng-computer-icons-avatar-social-media-blog-font-aweso-avatar-icon-5b2e99c4409623.4643918115297806762646.png")}
                  resizeMode="contain"
                  style={styles.image2}
                ></Image>
                <Image
                  source={require("../assets/images/kisspng-computer-icons-avatar-social-media-blog-font-aweso-avatar-icon-5b2e99c4409623.4643918115297806762646.png")}
                  resizeMode="contain"
                  style={styles.image3}
                ></Image>
              </View>
            </View>
            <Text style={styles.ultimosConteudos}>Últimos conteúdos</Text>
            <View style={styles.rect2}>
              <View style={styles.icon2Row}>
                <FontAwesomeIcon
                  name="book"
                  style={styles.icon2}
                ></FontAwesomeIcon>
                <View style={styles.sobreColumn}>
                  <Text style={styles.sobre}>Sobre</Text>
                  <Text style={styles.sobre2}>Alguns dados sobre a efi..</Text>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.loremIpsumColumnFiller}></View>
          <View style={styles.group1}>
            <View style={styles.rect4}>
              <View style={styles.endWrapperFiller}></View>
              <View style={styles.ellipseRow}>
                <Svg viewBox="0 0 13.22 13.22" style={styles.ellipse}>
                  <Ellipse
                    strokeWidth={1}
                    fill="rgba(230, 230, 230,1)"
                    stroke="rgba(230, 230, 230,1)"
                    cx={7}
                    cy={7}
                    rx={6}
                    ry={6}
                  ></Ellipse>
                </Svg>
                <Text style={styles.live}>LIVE</Text>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
      <View style={styles.rect5}>
        <MaterialHeader4
          button1="Home"
          text1="Home"
          style={styles.materialHeader4}
        ></MaterialHeader4>
        <View style={styles.group3}>
          <View style={styles.bemVindoPhillipe4Stack}>
            <Text style={styles.bemVindoPhillipe4}>Bem Vindo Phillipe!</Text>
            <Text style={styles.bemVindoPhillipe2}>
              Falta 2 dias para o início do evento.{"\n"}Você ja completou seu
              cadastro?
            </Text>
          </View>
        </View>
      </View>
      <View style={styles.scrollArea2}>
        <ScrollView
          horizontal={true}
          contentContainerStyle={styles.scrollArea2_contentContainerStyle}
        >
          <View style={styles.btnmenuRow}>
            <Btnmenu style={styles.btnmenu}></Btnmenu>
            <Btnmenu
              text1="Mapas"
              icon1Name="map"
              style={styles.btnmenu3}
            ></Btnmenu>
            <View style={styles.group2}>
              <View style={styles.rect6}>
                <IoniconsIcon name="md-eye" style={styles.icon3}></IoniconsIcon>
                <Text style={styles.live3}>Live</Text>
              </View>
            </View>
          </View>
          <View style={styles.btnmenu2Row}>
            <Btnmenu
              text1="Conteúdos"
              icon1Name="archive"
              style={styles.btnmenu2}
            ></Btnmenu>
            <Btnmenu
              text1="Chat"
              icon1Name="chat"
              style={styles.btnmenu4}
            ></Btnmenu>
          </View>
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(248,242,255,1)"
  },
  scrollArea: {
    width: 375,
    height: 359,
    backgroundColor: "#f8f2ff",
    marginTop: 453
  },
  scrollArea_contentContainerStyle: {
    width: 375,
    height: 1795,
    flexDirection: "column"
  },
  loremIpsum: {
    color: "rgba(116,23,201,1)",
    fontSize: 18,
    fontFamily: "Montserrat-Bold",
    marginLeft: 1
  },
  group: {
    width: 170,
    height: 48,
    flexDirection: "row",
    marginTop: 14
  },
  image: {
    width: 51,
    height: 48
  },
  image2: {
    width: 51,
    height: 48,
    marginLeft: 8
  },
  image3: {
    width: 51,
    height: 48,
    marginLeft: 9
  },
  imageRow: {
    height: 48,
    flexDirection: "row",
    flex: 1
  },
  ultimosConteudos: {
    color: "rgba(116,23,201,1)",
    fontSize: 18,
    fontFamily: "Montserrat-Bold",
    marginTop: 49,
    marginLeft: 1
  },
  rect2: {
    width: 318,
    height: 70,
    backgroundColor: "rgba(255,255,255,1)",
    borderRadius: 10,
    marginTop: 18,
    marginLeft: 1
  },
  icon2: {
    color: "rgba(128,128,128,1)",
    fontSize: 40,
    height: 40,
    width: 37
  },
  sobre: {
    color: "rgba(0,0,0,1)",
    fontSize: 15,
    fontFamily: "Montserrat-Regular"
  },
  sobre2: {
    color: "rgba(142,140,140,1)",
    fontSize: 14,
    fontFamily: "Montserrat-Regular",
    marginTop: 4
  },
  sobreColumn: {
    width: 178,
    marginLeft: 25,
    marginTop: 3,
    marginBottom: 4
  },
  icon2Row: {
    height: 40,
    flexDirection: "row",
    marginTop: 15,
    marginLeft: 19,
    marginRight: 59
  },
  loremIpsumColumn: {
    width: 319,
    marginTop: 13,
    marginLeft: 28
  },
  loremIpsumColumnFiller: {
    flex: 1
  },
  group1: {
    width: 319,
    height: 45,
    marginBottom: 43,
    marginLeft: 28
  },
  rect4: {
    height: 45,
    backgroundColor: "rgba(196,2,2,1)",
    borderRadius: 10,
    flexDirection: "row"
  },
  endWrapperFiller: {
    flex: 1,
    flexDirection: "row"
  },
  ellipse: {
    width: 13,
    height: 13,
    marginRight: 13,
    marginTop: 5
  },
  live: {
    color: "rgba(255,255,255,1)",
    fontSize: 22,
    fontFamily: "Montserrat-Bold"
  },
  ellipseRow: {
    height: 22,
    flexDirection: "row",
    marginRight: 7,
    marginTop: 11
  },
  rect5: {
    height: 228,
    backgroundColor: "#652c96",
    marginTop: -812
  },
  materialHeader4: {
    height: 56,
    marginTop: 32
  },
  group3: {
    width: 243,
    height: 61,
    marginTop: 36,
    marginLeft: 28
  },
  bemVindoPhillipe4: {
    top: 0,
    left: 0,
    color: "rgba(255,255,255,1)",
    position: "absolute",
    fontSize: 25,
    fontFamily: "Montserrat-Regular"
  },
  bemVindoPhillipe2: {
    top: 35,
    left: 0,
    color: "rgba(255,255,255,1)",
    position: "absolute",
    fontSize: 13,
    fontFamily: "Montserrat-Regular",
    lineHeight: 17
  },
  bemVindoPhillipe4Stack: {
    width: 243,
    height: 69
  },
  scrollArea2: {
    width: 375,
    height: 225,
    backgroundColor: "#f8f2ff"
  },
  scrollArea2_contentContainerStyle: {
    width: 1875,
    height: 225,
    flexDirection: "column"
  },
  btnmenu: {
    width: 90,
    height: 90
  },
  btnmenu3: {
    width: 90,
    height: 90,
    marginLeft: 18
  },
  group2: {
    width: 90,
    height: 90,
    marginLeft: 17
  },
  rect6: {
    width: 90,
    height: 90,
    backgroundColor: "rgba(255,255,255,1)",
    borderRadius: 5
  },
  icon3: {
    color: "rgba(208,2,27,1)",
    fontSize: 28,
    height: 28,
    width: 25,
    alignSelf: "flex-end",
    marginTop: 19,
    marginRight: 31
  },
  live3: {
    color: "#652c96",
    fontSize: 13,
    fontFamily: "Montserrat-SemiBold",
    textAlign: "center",
    marginTop: 9
  },
  btnmenuRow: {
    height: 90,
    flexDirection: "row",
    marginTop: 15,
    marginLeft: 29,
    marginRight: 41
  },
  btnmenu2: {
    width: 90,
    height: 90
  },
  btnmenu4: {
    width: 90,
    height: 90,
    marginLeft: 18
  },
  btnmenu2Row: {
    height: 90,
    flexDirection: "row",
    marginTop: 13,
    marginLeft: 29,
    marginRight: 148
  }
});

export default Home;
