import React, { useEffect, useState, useCallback } from 'react';
import { View, Text, StyleSheet, Keyboard, ScrollView, TouchableOpacity, Dimensions } from 'react-native';
import { useNavigation, useRoute, useFocusEffect } from '@react-navigation/native';
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import { GiftedChat, Bubble } from 'react-native-gifted-chat'
import YoutubePlayer from "react-native-yt-player";
import "moment";
import "moment/locale/pt-br";
import _ from 'lodash';
import messagesdemo from '../components/Messagesdemo'
import { useSafeArea, SafeAreaView } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/Ionicons';

//import { styles } from '../styles/styles';

 Liveevent = ({navigation}) => {

  const [messages, setMessages] = useState([])
  const [instance, setInstance] = useState(null)
  const [off, setOff] = useState(false)
  //const { content } = route.params;
  const [isfullscreen, setIsFullscreen] = useState(false)
  const insets = useSafeArea();

  useFocusEffect(
    React.useCallback(() => {
      setOff(true)
      // Do something when the screen is focused
      return () => {
        // Do something when the screen is unfocused
        // Useful for cleanup functions
        //console.log( myvideo)
        //setPause(true)
        setOff(false)
      };
    }, []),
  );
    
  useEffect(() => {
    //console.log(new Date())
    const sorted = _.orderBy(messagesdemo, ['createdAt'],['desc'])
    setMessages(sorted)

  },[])

  useEffect(() => {
   //console.log(messages)
   //console.log('data: ', new Date().toISOString())
  },[messages])
  
  useEffect(() => {
    
    if(instance){
      //console.log('TIMEOUT', instance)
      setTimeout(() => {
        /*console.log('SCROLL', instance._messageContainerRef.current)*/
        instance._messageContainerRef.current.scrollToIndex({index: messages.length-1, viewOffset: 0, viewPosition: 1});
      }, 1000);
    }

  },[instance])

  const onSend = useCallback((messages = []) => {
    Keyboard.dismiss()

    setMessages(previousMessages => GiftedChat.append(previousMessages, messages))

    setTimeout(() => {
      respostaAutomatica()
    }, 3000)
  }, [])
  
    /*onSend = (msg = []) => {
      setMessages(previousMessages => GiftedChat.append(previousMessages, msg))
      
      
      
    }*/
  
     TopBar = ({ play, fullScreen }) => {(
      <View
        style={{
          alignSelf: "center",
          position: "absolute",
          top: 0
        }}
      >
        <Text style={{ color: "#FFF" }}> Custom Top bar</Text>
      </View>
    )}

    onFullScreen = (e) => {
      setIsFullscreen(e)
    }

    openStuff = (e) => {
      console.log(e)
      navigation.navigate('Quiz', { screenName: "Quiz" })
    }

    endVideo = () => {
      //alert("on End")
      const newmess = _.clone(messages)
      newmess.push({
        _id: 10,
        text: 'Obrigado por assistir a LIVE! /n Clique para jogar!',
        createdAt: '2020-06-13T00:21:49.126Z',
        
        quickReplies: {
          type: 'radio', // or 'checkbox',
          keepIt: true,
          values: [
            {
              title: 'Jogar',
              value: 'yes',
            },
          ],
        },
        user: {
          _id: 2,
          name: 'Antidoto',
          avatar: 'http://www.antidotodesign.com.br/dev/talk/antidoto-logo-preto.JPG',
        },
      })

      
      
      const sorted = _.orderBy(newmess, ['createdAt'],['desc'])
      setMessages(sorted)
     
      instance && (instance._messageContainerRef.current.scrollToIndex({index: 0, viewOffset: 0, viewPosition: 1}))
      
    }

    respostaAutomatica = () => {
      const newmess = _.clone(messages)
      newmess.push({
        _id: messages.length,
        text: 'Obrigado pela pergunta! Esse chat esta em modo demonstação, as perguntas não serão enviadas',
        createdAt: new Date(),
        user: {
          _id: 2,
          name: 'Antidoto',
          avatar: 'http://www.antidotodesign.com.br/dev/talk/antidoto-logo-preto.JPG',
        },

      })

      const sorted = _.orderBy(newmess, ['createdAt'],['desc'])
      setMessages(sorted)

      console.log(sorted)
    
      instance && (instance._messageContainerRef.current.scrollToIndex({index: 0, viewOffset: 0, viewPosition: 1}))
    }

    renderBubble = props => {
      //let username = props.currentMessage.user.name
      //let color = this.getColor(username)
  
      return (
        <Bubble
          {...props}
          textStyle={{
            right: {
              color: 'white'
            }
          }}
          wrapperStyle={{
            right: {
              backgroundColor: 'black'
            }
          }}
        />
      )
    }


  return (
    <View style={[styles.container, {
      paddingTop: insets.top,
      //paddingBottom: insets.bottom
    }]}>
        { !isfullscreen && (<TouchableOpacity 
            onPress={() => navigation.goBack()}
            style={{width:Dimensions.get('window').width-20, flexDirection:'row', margin:10}}
        >
            <Icon 
                name="ios-arrow-back" 
                color={'#000'}
                size={25}
            />
            <Text style={{fontSize:20, marginLeft:10}}>Voltar</Text>
        </TouchableOpacity>
        )}
        
        
        <View style={styles.video}>
          {off && (
            <ScrollView style={{width:'100%'}}>
          <YoutubePlayer
            topBar={TopBar}
            videoId={'FowPvcE_8ag'}
            autoPlay
            onFullScreen={onFullScreen}
            onStart={() => console.log("onStart")}
            onEnd={() => endVideo()}
          />
          </ScrollView>
          )} 
        </View>
        
        { !isfullscreen && (
        <GiftedChat
            ref={c => 
                setInstance(c)
            }
            messages={messages}
            renderBubble={renderBubble}
            onSend={messages => onSend(messages)}
            onQuickReply = {e => openStuff(e)}
            renderUsernameOnMessage
            locale='pt-br'
            infiniteScroll
            user={{
            _id: 1,
            }}
        />
        )} 
        
</View>

);
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#f8f2ff"
    },
    rect1: {
      width: '100%',
      height: 150,
      backgroundColor: "#652c96"
    },
    materialHeader1: {
      width: '100%',
      height: 56,
      marginTop: 32
    },
    rect2: {
      width: 365,
      height: 93,
      backgroundColor: "rgba(255,255,255,1)",
      borderRadius: 8,
      marginTop: 25,
      marginLeft: 23
    },
    palestra: {
      color: "rgba(65,117,5,1)",
      fontSize: 18,
      fontFamily: "Montserrat-Medium",
      lineHeight: 17,
      marginTop: 24,
      marginLeft: 24
    },
    icon: {
      color: "rgba(155,155,155,1)",
      fontSize: 15,
      height: 15,
      width: 11
    },
    johnDoe: {
      color: "rgba(155,155,155,1)",
      fontSize: 12,
      fontFamily: "Montserrat-Medium",
      lineHeight: 17,
      marginLeft: 10
    },
    icon2: {
      color: "rgba(155,155,155,1)",
      fontSize: 15,
      height: 15,
      width: 13,
      marginLeft: 47
    },
    johnDoe2: {
      color: "rgba(155,155,155,1)",
      fontSize: 12,
      fontFamily: "Montserrat-Medium",
      lineHeight: 17,
      marginLeft: 7
    },
    iconRow: {
      height: 17,
      flexDirection: "row",
      marginTop: 13,
      marginLeft: 24,
      marginRight: 86
    }
  });

export default Liveevent;