import React from 'react'
import {View, Image, StyleSheet} from 'react-native'

export default Splash = () => {
    return <View style={{flex:1, justifyContent: 'center'}}>
        <Image
              source={require("../assets/images/logo_antidoto.png")}
              style={styles.infoimage}
              resizeMode="contain"
            ></Image>
        </View>
}

const styles = StyleSheet.create({

    infoimage: {
        flex:1,
        height: undefined, 
        width: undefined,
        alignSelf: 'stretch',
        maxHeight:50,
        alignContent:'center'
        //width: 400,
        //height: 400,
        //marginLeft: 9
      },
})