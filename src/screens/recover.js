import React, { useContext, useReducer, useEffect } from 'react';
import { View, Text, StyleSheet, Button, Platform, TouchableOpacity, Image } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';

import Recoverform from "../components/Recoverform";
import MaterialHelperTextBox1 from "../components/MaterialHelperTextBox1";
import MaterialSwitch from "../components/MaterialSwitch";
import MaterialButtonViolet from "../components/MaterialButtonViolet";
import MaterialButtonWithShadow from "../components/MaterialButtonWithShadow";



import UUIDGenerator from 'react-native-uuid-generator';

//import { Gun } from '../../UserContext';
import {Gun} from '../helpers/hooks';

//import { styles } from './styles/styles';

//AsyncStorage.clear();

 UUIDGenerator.getRandomUUID((uuid) => {
  //console.log(uuid);
});

Recover = () => {

  
  const navigation = useNavigation();
  const route = useRoute();
  const gun = useContext(Gun)

  

  let detailResult = route.params;
  return (
    
    <React.Fragment>
      
        <View style={styles.topScreen}>
          <View style={styles.boxText}>
            <Text style={styles.textHeader}>Recupere sua senha</Text>
          </View>
        </View>
    
        <View style={styles.items}>
          <View style={styles.login}>
            <Recoverform />
          </View>
        </View> 

    </React.Fragment>

   
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  topScreen: {
    flex:1,
    backgroundColor: "#000",
    justifyContent:'flex-end'
  },
  items:{
    flex: 5,
    //flexDirection: "column",
    //marginTop:200,
    //alignItems: 'center',
  },
  login: {
    flex:1,
    justifyContent: 'space-around',
    alignItems: 'center',
    margin:30
  },
  livelo: {
    width: 100,
    height: 100,
  },
  boxText: {
    width: '100%',
    marginLeft:30,
    marginBottom:20
  },
  textHeader: {
    height:35,
    color: "rgba(255,255,255,1)",
    fontSize: 30,
    fontFamily: "Montserrat-Regular"
  },
  textSubHeader: {
    height:40,
    color: "rgba(255,255,255,1)",
    fontSize: 15,
    fontFamily: "Montserrat-Regular",
  },
});

export default Recover;