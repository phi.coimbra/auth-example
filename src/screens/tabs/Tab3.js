import React, {useEffect, useState} from 'react';
import { View, Text } from 'react-native';

import { styles } from '../../styles/styles.js'

import {  NodeCameraView } from 'react-native-nodemediaclient';

Tab3 = () => {

  const [strm, setStrm] = useState(null)

  useEffect(() => {
    strm?strm.start():[]
   // setTimeout(() => {
      strm?console.log('strm', strm):[]
      //strm.start()
      
  //  },3000)
  },[strm])

  return (
  <View style={styles.center}>
    <Text style={styles.title}>Mapas</Text>
    <NodeCameraView 
          style={{ height: 400 }}
          ref={(vb) =>  setStrm(vb) }
          outputUrl = {"rtmp://a.rtmp.youtube.com/live2/tjw4-13ae-qqqh-sywc-2bby"}
          camera={{ cameraId: 1, cameraFrontMirror: true }}
          audio={{ bitrate: 32000, profile: 1, samplerate: 44100 }}
          video={{ preset: 1, bitrate: 400000, profile: 1, fps: 15, videoFrontMirror: false }}
          autopreview={true}
        />
  </View>
  )

}

export default Tab3;