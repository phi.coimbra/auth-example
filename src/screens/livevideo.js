import React, {useState, useEffect} from 'react';
import { View, Text, StyleSheet, Dimensions, ScrollView, TouchableOpacity } from 'react-native';
import { useNavigation, useRoute, useIsFocused, useFocusEffect } from '@react-navigation/native';
import Catalogo from "../components/Catalogo";
import Video from 'react-native-video';
import YoutubePlayer from "react-native-yt-player";
import { useSafeArea, SafeAreaView } from 'react-native-safe-area-context';
import {  NodePlayerView } from 'react-native-nodemediaclient';
import Icon from 'react-native-vector-icons/Ionicons';

//import { styles } from '../styles/styles';

Livevideo = ({ route, navigation }) => {

  const [myvideo, setMyvideo] = useState()
  const [paused, setPause] = useState(true)
  const [off, setOff] = useState(false)
  const insets = useSafeArea();

  const isFocused = useIsFocused();

  useFocusEffect(
    React.useCallback(() => {
      setOff(true)
      // Do something when the screen is focused
      return () => {
        // Do something when the screen is unfocused
        // Useful for cleanup functions
        //console.log( myvideo)
        //setPause(true)
        setOff(false)
      };
    }, []),
  );


  /*React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      // The screen is focused
      // Call any action
      console.log(isFocused ? 'focused' : 'unfocused')
    });

    console.log(isFocused ? 'focused' : 'unfocused')
    // Return the function to unsubscribe from the event so it gets removed on unmount
    return console.log('ok');
  }, [navigation]);*/



  useEffect(() => {
   
    if(myvideo){
     // console.log(myvideo.props.paused)
      //setPause(true)
      
      //!isFocused? myvideo.volume = 0: myvideo.volume = 1
    }
    
    
    // Atualiza o título do documento utilizando a API do navegador
    //console.log(myvideo)
  });

  const TopBar = ({ play, fullScreen }) => (
    <View
      style={{
        alignSelf: "center",
        position: "absolute",
        top: 0
      }}
    >
      <Text style={{ color: "#FFF" }}>Custom Top bar</Text>
    </View>
  )


  return (
    <View style={[styles.container, {
      paddingTop: insets.top,
      margin:20
      //paddingBottom: insets.bottom
    }]}>
        <View style={{height:50}}>
            <TouchableOpacity 
                onPress={() => navigation.goBack()}
                style={{width:Dimensions.get('window').width-20, flex:1, flexDirection:'row'}}
            >
                <Icon 
                    name="ios-arrow-back" 
                    color={'#000'}
                    size={25}
                />
                <Text style={{fontSize:20, marginLeft:10}}>Voltar</Text>
            </TouchableOpacity>
        </View>
        
        <View style={[styles.video, {flex:1, alignItems:'center'}]}>
          <Text style={{flex:1}}>Não existe nenhuma live no momento</Text>
        </View>
  </View>
    
  );
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
   
  },
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,

  },
  chat:{
    height:'65%'
  },
  video:{
    height:'35%'
  }
})

export default Livevideo;