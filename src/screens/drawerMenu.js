import React, {useContext, useEffect, useState} from 'react';

import { getUniqueId, getSystemName } from 'react-native-device-info'

import { View, StyleSheet } from 'react-native';
import {
    useTheme,
    Avatar,
    Title,
    Caption,
    Paragraph,
    Drawer,
    Text,
    TouchableRipple,
    Switch
} from 'react-native-paper';
import {
    DrawerContentScrollView,
    DrawerItem
} from '@react-navigation/drawer';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon2 from 'react-native-vector-icons/MaterialIcons';

import { Dispatch, logout, Auth, DB } from '../helpers/hooks';



//import{ AuthContext } from '../components/context';

DrawerContent = (props) => {

    const paperTheme = useTheme();
    const [profile, setProfile] = useState(null)
    const reducer = useContext(Dispatch)
    const db = useContext(DB)
    const clientAuth = useContext(Auth)
    const uniqueID = getUniqueId();
    const [myData, setData] = useState({nome:null, email:null, image:null})

    //const URL_SERVER = 'http://192.168.1.48:3030/images/'
    const URL_SERVER = 'http://talk.antido.to:3030/images/'

    const [{logged, user}, dispatch] = reducer;

    useEffect(() => {

        db['users'].update$.subscribe(changeEvent => {
            if(user){
                if(changeEvent.documentData.email === user.email){
                    dispatch({type:'userdata', payload:changeEvent.documentData})
                }
            }
        });
        
        user && (
        setData(prev => ({ 
            ...prev,
            nome:user.nome + ' ' + user.sobrenome,
            email:user.email,
            image:user.image 
        })))

       /* gun.get('devices').get(uniqueID).get('dados').once(item => {
            //console.log('mail:', item.email)
            setData(prev => ({ 
                ...prev,
                email:item.email  
            }))
        })

        gun.get('devices').get(uniqueID).get('perfil').once(item => {
            if(item){
                setData(prev => ({ 
                    ...prev,
                    nome:item.nome + ' ' + item.sobrenome,
                    image:item.image 
                }))
            }else{
                setData(prev => ({ 
                    ...prev,
                    nome:' ',
                    image:'http://www.antidotodesign.com.br/dev/talk/avatar.PNG'
                }))
            }
        })*/
        
    },[user])

    function signOut(){
        logout(clientAuth);
        dispatch({type:'logout'}); 
    }

    return(
        <View style={{flex:1}}>
         
            <DrawerContentScrollView {...props}>
                <View style={styles.drawerContent}>
                    <View style={styles.userInfoSection}>
                        {myData.nome && (<View style={{flexDirection:'row',marginTop: 15}}>
                            <Avatar.Image 
                                source={{
                                    uri: myData.image
                                }}
                                size={50}
                            />
                           {myData.nome && ( <View style={{marginLeft:15, flexDirection:'column'}}>
                                <Title style={styles.title}>{myData.nome}</Title>
                                <Caption style={styles.caption}>{myData.email.substr(myData.email.indexOf('@'), myData.email.length)}</Caption>
                            </View>
                           )}
                        </View>
                        )}

                        {/*<View style={styles.row}>
                            <View style={styles.section}>
                                <Paragraph style={[styles.paragraph, styles.caption]}>80</Paragraph>
                                <Caption style={styles.caption}>Following</Caption>
                            </View>
                            <View style={styles.section}>
                                <Paragraph style={[styles.paragraph, styles.caption]}>100</Paragraph>
                                <Caption style={styles.caption}>Followers</Caption>
                            </View>
                            </View>*/}
                    </View>

                    <Drawer.Section style={styles.drawerSection}>
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name="home-outline" 
                                color={color}
                                size={size}
                                />
                            )}
                            label="Home"
                            onPress={() => {props.navigation.navigate('Homes')}}
                        />
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name="account-outline" 
                                color={color}
                                size={size}
                                />
                            )}
                            label="Perfil"
                            onPress={() => {props.navigation.navigate('Perfil')}}
                        />
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name="calendar" 
                                color={color}
                                size={size}
                                />
                            )}
                            label="Agenda"
                            onPress={() => {props.navigation.navigate('Agenda')}}
                        />
                       { /*<DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name="chat" 
                                color={color}
                                size={size}
                                />
                            )}
                            label="Chat"
                            onPress={() => {props.navigation.navigate('Chat')}}
                        />
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name="youtube-tv" 
                                color={color}
                                size={size}
                                />
                            )}
                            label="Live"
                            onPress={() => {props.navigation.navigate('Liveevent')}}
                        />
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name="content-paste" 
                                color={color}
                                size={size}
                                />
                            )}
                            label="Conteudo"
                            onPress={() => {props.navigation.navigate('Conteudo')}}
                        />
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon2 
                                name="perm-device-information" 
                                color={color}
                                size={size}
                                />
                            )}
                            label="Quiz"
                            onPress={() => {props.navigation.navigate('Quiz')}}
                        />
                            */}
                    </Drawer.Section>
                    
                </View>
            </DrawerContentScrollView>
            <Drawer.Section style={styles.bottomDrawerSection}>
                <DrawerItem 
                    icon={({color, size}) => (
                        <Icon 
                        name="exit-to-app" 
                        color={color}
                        size={size}
                        />
                    )}
                    label="Sign Out"
                    onPress={() => {signOut()}}
                />
            </Drawer.Section>
        </View>
    );
}

const styles = StyleSheet.create({
    drawerContent: {
      flex: 1,
    },
    userInfoSection: {
      paddingLeft: 20,
    },
    title: {
      fontSize: 16,
      marginTop: 3,
      color:'white',
      fontWeight: 'bold',
    },
    caption: {
      fontSize: 14,
      lineHeight: 14,
      color:'white',
    },
    row: {
      marginTop: 20,
      flexDirection: 'row',
      alignItems: 'center',
    },
    section: {
      flexDirection: 'row',
      alignItems: 'center',
      marginRight: 15,
    },
    paragraph: {
      fontWeight: 'bold',
      marginRight: 3,
    },
    drawerSection: {
      marginTop: 15,
    },
    bottomDrawerSection: {
        marginBottom: 15,
        borderTopColor: '#f4f4f4',
        borderTopWidth: 1
    },
    preference: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingVertical: 12,
      paddingHorizontal: 16,
    },
  });

  export default DrawerContent;