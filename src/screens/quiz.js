import React, { useState, useEffect, useContext, useRef } from 'react';
import { View, Text, StyleSheet, Button, ScrollView, TouchableOpacity, Image, Dimensions } from 'react-native';
import { useNavigation, useIsFocused, useFocusEffect } from '@react-navigation/native';
import Dot from "react-native-vector-icons/FontAwesome";
import { DB, Dispatch } from '../helpers/hooks';
import "moment";
import "moment/locale/pt-br";
import _ from "lodash";
import Icon from 'react-native-vector-icons/Ionicons';
import Animated from 'react-native-reanimated';
import { PanGestureHandler, State} from 'react-native-gesture-handler'

import  BtnQuiz  from '../components/Btnquiz'

//import { styles } from '../styles/styles';

/*const resp = [
  ['Metus aliquam eleifend mi in nulla posuere sollicitudin.', true, false], 
  ['Ante in nibh mauris cursus mattis molestie a iaculis at.', false, false], 
  ['Tortor at risus viverra adipiscing', false, false], 
  ['Ac turpis egestas integer eget aliquet nibh praesent.', false, false]
]*/

function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}


const Quiz = ({navigation, props}) => {

  const {set, cond, eq, add, spring, startClock, stopClock, clockRunning, sub, defined, Value, Clock, event } = Animated;
  const transX = new Value(100)

  const db = useContext(DB)
  
  const {width, height} = Dimensions.get('window')

  const reducer = useContext(Dispatch)
  const [{logged, cliente, user}, dispatch] = reducer;

  //const quizNode = gun.get(cliente).get('games').get('quiz');

  const [state, setState] = useState({ client:null, end:false, pergunta:null, allRespostas: null, totalPerguntas: null, perguntaAtual: null, ready: null, respondidas: null});
  const { pergunta, allRespostas, totalPerguntas, perguntaAtual, ready, respondidas, end } = state
  const prevCount = usePrevious(state)

  const [nada, nadal] = useState(false)

  
  

  useFocusEffect(
    React.useCallback(() => {
      //setOff(true)
      console.log('FOCUSED')
      
      return () => {
      /*  let respClone = [...respostas]
        console.log('NOT FOCUSED')
        respClone[0][2] = false
        respClone[1][2] = false
        respClone[2][2] = false
       // setRespostas(respClone)*/
        
      };
    }, []),
  );

  useEffect(() => {
    const { perguntaAtual, pergunta } = state

    console.log('gera pergunta')

    if(!_.isNil(perguntaAtual) && perguntaAtual !== prevCount.perguntaAtual){
      getPergunta();
    }

    if(!_.isNil(perguntaAtual) && pergunta !== prevCount.pergunta){
      getRespostas(perguntaAtual)
    }
    
    console.log( 'CHANGED: ', state/*, prevCount*/ )
    
  },[state])

  useEffect(() => {
    getConfig()
    //console.log( 'GET PERGUNTA ATUAL:', state.perguntaAtual)
  //  perguntaAtual !== null? getPergunta() : []
  },[])
  

  getConfig = async () => {
    console.log('CONFIG')
    return new Promise((resolve)=> {
      //const quizNode = gun.get(cliente).get('games').get('quiz');
 
     /* db['quiz'].findOne().where('evento').eq(user.evento).exec().then(doc => {
        console.log('teste', doc)
      })*/
      db['quiz'].findOne().where('evento').eq(user.evento).exec().then(doc => {
        
        //console.log(doc)
       let values = []
       if(doc){
          _.times(4, () => values.push(['circle', 'black']))
          console.log( 'SET PERGUNTA')

          setState(prev => ({ 
            ...prev,
            respondidas:values,
            totalPerguntas:doc.perguntas,
            perguntaAtual: 0
        }));
          resolve()
       }else{
          console.log('BUGGED CONFIG')
          resolve()
         // getConfig()
       }
      })
    })
    
  }

  function getPergunta(){
    console.log('PERGUNTA ---------------------s')
    //const quizNode = gun.get('games').get('quiz');
   /* quizNode.get('quiz1').get('pergunta/'+perguntaAtual).get('respostas').map().once(resposta => {
      console.log('BUGGED',resposta)
    })*/

   // console.log('BUGGED PERGUNTA ATUAL',  perguntaAtual)

    
          console.log('PEGA PERGUNTA', totalPerguntas[perguntaAtual])

          setState(prev => ({ 
            ...prev,
            pergunta:totalPerguntas[perguntaAtual].titulo
          }));

          //setPergunta(pergunta.titulo)
          //const path = _.get(pergunta.respostas, '#');
          
        
      
  
  }

  function getRespostas(perguntaAtual){
    console.log('RESPOSTAS ---------------------')
    const resp = []
    //const quizNode = gun.get('games').get('quiz');
   for(let a = 0; a<totalPerguntas[perguntaAtual].alternativas.length; a++){
        
        resp.push([totalPerguntas[perguntaAtual].alternativas[a].titulo, Number(totalPerguntas[perguntaAtual].resposta) === a?true:false, false, false])

        console.log('POPULA RESPOSTA CERTA')

        if(resp.length === totalPerguntas.length+1){
          console.log('POPULA RESPOSTA', resp)
          //let cloned = _.clone(resp)
          setState(prev => ({ 
            ...prev,
            allRespostas:resp,
            ready:true
          }));
          //setRespostas(cloned)
          //setReady(true)
        }
     // }
    }
  }

  function setResposta(r, select){
    console.log('RESPONDENDO')
    
    if(allRespostas[r][1] === true){
      respondidas[perguntaAtual][0] = 'check'
      respondidas[perguntaAtual][1] = 'green'
    }else{
      respondidas[perguntaAtual][0] = 'close'
      respondidas[perguntaAtual][1] = 'red'
    }
   // if(allRespostas[r][1] === true){
    //  select(true);
   // }else{
     let respClone = [...allRespostas]
      for(let i = 0; i<respClone.length; i++){
        if(respClone[i][1] === true){
          respClone[i][2] = true
          respClone[r][2] = true
          
          
        }
        respClone[i][3] = true
      }
      setState(prev => ({ 
        ...prev,
        allRespostas:respClone
      }));
   // }
    //nadal('nada')
    //const mResp = _.clone(respondidas)
    
    
    
    //nadal('nada')
    //setRespondidas(mResp)
    

    setTimeout(()=>{
      //setRespondidas(mResp)
      if(perguntaAtual !== totalPerguntas.length+1){
        //setRespostas(null)
        if(perguntaAtual !== 3){
          setState(prev => ({ 
            ...prev,
            perguntaAtual:perguntaAtual+1
          }));
          //perguntaAtual = perguntaAtual+1
        }else{
          setState(prev => ({ 
            ...prev,
            end:true,
            ready:false
          }));

          setTimeout(()=>{
            navigation.goBack()
          },8000)

          //respondidas[perguntaAtual][0] = 'check'
          //respondidas[perguntaAtual][1] = 'green'
          //setPerguntaAtual(0)
        }
        //nadal(perguntaAtual)
        
      }
    },2000)

   
  }

  const SetAllResposta = () => {
    console.log('BOTOES')
    return allRespostas.map((resp, item) => {
      return <BtnQuiz id={item} right={allRespostas[item][1]} selected={allRespostas[item][2]} resposta={allRespostas[item][0]} responde={setResposta} posicao={transX} answered={allRespostas[item][3]}/>
    })
  }

  const GetIcons = () => {
    console.log('ICONS')
    return respondidas.map((dot, index) => {
      return <Dot 
        name={dot[0]} // close dot-single check
        color={dot[1]}
        size={dot[0] !== 'circle'?25:10}
        style={{width:30, textAlign:'center'}}
      />
      
    })
  }

  Ranking = () => {
    return (
      <Animated.View style={{flex:1, marginTop:50}}>
        <View style={{flexDirection:'row', backgroundColor:'#fff', justifyContent:'space-between'}}>
          <View style={{margin:10}}>
            <Text >Posição</Text>
          </View>
          <View style={{margin:10}}>
            <Text>Nome</Text>
          </View>
          <View style={{margin:10}}>
            <Text>Pontuação</Text>
          </View>
        </View>
        <View style={{flexDirection:'row', backgroundColor:'#eee', justifyContent:'space-between'}}>
          <View style={{margin:10}}>
            <Text >1</Text>
          </View>
          <View style={{margin:10}}>
            <Text>Usuário</Text>
          </View>
          <View style={{margin:10}}>
            <Text>1200</Text>
          </View>
        </View>
        <View style={{flexDirection:'row', backgroundColor:'#e6e6e6', justifyContent:'space-between'}}>
          <View style={{margin:10}}>
            <Text >2</Text>
          </View>
          <View style={{margin:10}}>
            <Text>Usuário 2</Text>
          </View>
          <View style={{margin:10}}>
            <Text>1000</Text>
          </View>
        </View>
        <View style={{flexDirection:'row', backgroundColor:'#eee', justifyContent:'space-between'}}>
          <View style={{margin:10}}>
            <Text >3</Text>
          </View>
          <View style={{margin:10}}>
            <Text>Usuário 3</Text>
          </View>
          <View style={{margin:10}}>
            <Text>800</Text>
          </View>
        </View>
        <View style={{flexDirection:'row', backgroundColor:'#e6e6e6', justifyContent:'space-between'}}>
          <View style={{margin:10}}>
            <Text >4</Text>
          </View>
          <View style={{margin:10}}>
            <Text>Usuário 4</Text>
          </View>
          <View style={{margin:10}}>
            <Text>600</Text>
          </View>
        </View>
        
      </Animated.View>
    )
  }
  
  return(
    
    <Animated.View style={[{...StyleSheet.absoluteFill}, {flex:1, backgroundColor:'#efefef'}]}>

      
      <View style={{backgroundColor:'#ffcc30', height:height/4}}>
      {!end && (
      <TouchableOpacity 
          onPress={() => navigation.goBack()}
          style={{width:Dimensions.get('window').width-20, flexDirection:'row', margin:20}}
      >
          <Icon 
              name="ios-arrow-back" 
              color={'#000'}
              size={25}
          />
          <Text style={{fontSize:20, marginLeft:10}}>Voltar</Text>
      </TouchableOpacity>
      )}
        {ready && (<View style={{flex:1, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
          <React.Fragment>
            {GetIcons()}
          </React.Fragment>
        </View>)}
        <View style={{flex:3, alignItems:'center', justifyContent:'center',width:width-30, marginLeft:15, marginRight:15}}>
          
  {ready && (<Text style={{fontSize:20, color: "black", width:'100%', textAlign:'center'}}>{pergunta}</Text>)}
        {end && (<Text style={{fontSize:20, color: "black", margin:30}}>Obrigado pela participação, veja sua pontuação abaixo!</Text>)}
        </View>
      </View> 
      <Animated.View style={{flex:1, backgroundColor:'transparent', margin:30, justifyContent:'space-around' }}>
      
        {ready && (<SetAllResposta />)}
        {end && (<Dot 
                  name={'trophy'} // close dot-single check
                  color={'black'}
                  size={100}
                  style={{textAlign:'center'}}
        />)}
        {end && (<Ranking />)}
      </Animated.View>
    </Animated.View>
  )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#f8f2ff"
    },
    rect1: {
      width: '100%',
      height: 150,
      backgroundColor: "#652c96"
    },
    materialHeader1: {
      width: '100%',
      height: 56,
      marginTop: 32
    },
    rect2: {
      width: 365,
      height: 93,
      backgroundColor: "rgba(255,255,255,1)",
      borderRadius: 8,
      marginTop: 25,
      marginLeft: 23
    },
    palestra: {
      color: "rgba(65,117,5,1)",
      fontSize: 18,
      fontFamily: "Montserrat-Medium",
      lineHeight: 17,
      marginTop: 24,
      marginLeft: 24
    },
    icon: {
      color: "rgba(155,155,155,1)",
      fontSize: 15,
      height: 15,
      width: 11
    },
    johnDoe: {
      color: "rgba(155,155,155,1)",
      fontSize: 12,
      fontFamily: "Montserrat-Medium",
      lineHeight: 17,
      marginLeft: 10
    },
    icon2: {
      color: "rgba(155,155,155,1)",
      fontSize: 15,
      height: 15,
      width: 13,
      marginLeft: 47
    },
    johnDoe2: {
      color: "rgba(155,155,155,1)",
      fontSize: 12,
      fontFamily: "Montserrat-Medium",
      lineHeight: 17,
      marginLeft: 7
    },
    iconRow: {
      height: 17,
      flexDirection: "row",
      marginTop: 13,
      marginLeft: 24,
      marginRight: 86
    },
    qrcode: {
      width: 400,
      height: 400,
      marginLeft: 9
    },
  });

export default Quiz;