import React , {useEffect, useContext, useState} from 'react';
import { View, Text, StyleSheet, Button, Platform, TouchableOpacity,ScrollView, Image, Dimensions } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import { Dispatch, logout, Auth, DB, userData } from '../helpers/hooks';
import { getUniqueId, getSystemName } from 'react-native-device-info'

import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";

import { styles } from '../styles/styles';

Home = () => {

  const [profileNome, setProfileNome] = useState('')
  const db = useContext(DB)
  const uniqueID = getUniqueId();
  const {width, height} = Dimensions.get('window')
  const reducer = useContext(Dispatch)
  const [{user}, dispatch] = reducer;
  const [sub, setSub] = useState(null)

  useEffect(() => {
    return () => {
      if(sub){
        sub.unsubscribe()
      } 
    }
  },[])

  useEffect(() => {
    console.log('user mudou: ', user);
      if(user){
        console.log('user tem ', user);
        if(!user.nome){
          console.log('user nao nome ', user);
          findUser();
        } 
      }
    
  },[user])

  findUser = () =>{
    db['users'].findOne().where('email').eq(user.email).exec().then(doc => {
      // console.log('resposta: ', doc);
      if(doc){
        console.log('encontrou')
        if(sub){
          sub.unsubscribe()
        } 
       setProfileNome(doc.nome)
       dispatch({type:'userdata', payload:doc})
      }else{
        console.log('nada ainda')
        subscUser()
      }
     })
  }

  subscUser = () => {
    const sub = db['users'].insert$.subscribe(changeEvent => {
      console.log('subscribed')
      findUser();
    })
    setSub(sub)
  }

  return (
    <View style={styles.container}>
      <View style={styles.rect5}>
        <View style={styles.group3}>
          <View style={styles.bemVindoPhillipe4Stack}>
            <Text style={stylesin.bemvindo}>Bem Vindo {profileNome}!</Text>
            <Text style={stylesin.info}>
              Falta 2 dias para o início do evento.{"\n"}Você ja completou seu
              cadastro?
            </Text>
          </View>
        </View>
      </View>
      {/*<View style={stylesin.imagecontainer}>*/}
      
      {/*<Image
        source={require("../assets/images/talkinfo.png")}
        style={stylesin.infoimage}
        resizeMode="contain"
      ></Image>*/}
       <View style={stylesin.container}>
          <View style={stylesin.imageContainer}>
              <Image
                  source={require("../assets/images/talkinfo.png")}
                  style={stylesin.largeImage}
                  resizeMode='contain'
              />
          </View>
      </View>
      
     
    </View>
    
  );
}

const stylesin = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#eeee',
    justifyContent:"flex-end"
  },
  imageContainer: {
      width: '100%',
      height: '84%',
      backgroundColor: '#eee',
      justifyContent: "flex-end"
  },
  largeImage: {
      width: '100%',
      height:"100%"
  } ,

  imagecontainer:{
    flex:1,
    flexDirection:'column',
    backgroundColor:'#eee',
    justifyContent:'flex-end',
    
    
  },
  infoimage: {
    flex:1,
    //height:80,
    justifyContent: 'flex-end',
    //alignItems:'center', 
    backgroundColor:'#ff00',
    alignSelf:'flex-end',
    alignItems:'flex-end',
   
   // resizeMode:'contain'
    //marginTop:90,
    
    //width: 400,
    //marginLeft: 20,
    //marginRight: 20
  },

  viewStyleOne: {
    flex:1,
    //height:80,
    //marginLeft:20,
    alignSelf:'flex-end',
    justifyContent: 'flex-end',
    alignItems:'flex-end', 
    backgroundColor:'#ff00',
    position: 'absolute',
    top: 0
  },

  bemvindo: {

    color: "#121212",
    fontSize: 24,
    fontFamily: "Montserrat-Regular",
    //lineHeight: 20,
    //marginTop: 10,
    //textAlign: "center"
  },
  info: {

    color: "#121212",
    fontSize: 14,
    fontFamily: "Montserrat-Regular",
    //lineHeight: 20,
    //marginTop: 10,
    //textAlign: "center"
  },


})


export default Home;