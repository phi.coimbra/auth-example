import React from 'react';
import { StyleSheet, Dimensions, View, TouchableOpacity, Text } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { useSafeArea, SafeAreaView } from 'react-native-safe-area-context';
import Pdf from 'react-native-pdf';

contentPdf = ({navigation, route}) => {
   
    const { content } = route.params;
    const insets = useSafeArea();
    //const source = {uri:content ,cache:true};
    //const source = content.name;
    //const source = require('./test.pdf');  // ios only
    //const source = {uri:'bundle-assets://test.pdf'};

    //const source = {uri:'file:///sdcard/test.pdf'};
    const source = {uri:content};

    return (
        <View style={[styles.container, {
            paddingTop: insets.top,
            margin:30
            //paddingBottom: insets.bottom
        }]}>
            <View style={{height:50}}>
            <TouchableOpacity 
                onPress={() => navigation.goBack()}
                style={{width:Dimensions.get('window').width-20, flex:1, flexDirection:'row'}}
            >
                <Icon 
                    name="ios-arrow-back" 
                    color={'#000'}
                    size={25}
                />
                <Text style={{fontSize:20, marginLeft:10}}>Voltar</Text>
            </TouchableOpacity>
            </View>
            <Pdf
                source={source}
                onLoadComplete={(numberOfPages,filePath)=>{
                    console.log(`number of pages: ${numberOfPages}`);
                }}
                onPageChanged={(page,numberOfPages)=>{
                    console.log(`current page: ${page}`);
                }}
                onError={(error)=>{
                    console.log(error);
                }}
                onPressLink={(uri)=>{
                    console.log(`Link presse: ${uri}`)
                }}
                style={styles.pdf}
            />
        </View>
    )
  
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 25,
    },
    pdf: {
        flex:1,
        width:'100%',
        height:Dimensions.get('window').height,
    }
});

export default contentPdf