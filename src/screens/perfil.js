import React from 'react';
import { View, Text, StyleSheet, Button, ScrollView, TouchableOpacity, Image } from 'react-native';

import "moment";
import "moment/locale/pt-br";

//import { styles } from '../styles/styles';

class Perfil extends React.Component {
    state = {
      messages: [], 
    }
  
    
  
    render() {
  return (
    <View style={styles.container}>
      
          <Text style={styles.palestra}>PERFIL</Text>
          <Image
                  source={require("../assets/images/qrcode.png")}
                 
                  style={styles.qrcode}
                ></Image>
          
    </View>
    
  );
}
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#f8f2ff"
    },
    rect1: {
      width: '100%',
      height: 150,
      backgroundColor: "#652c96"
    },
    materialHeader1: {
      width: '100%',
      height: 56,
      marginTop: 32
    },
    rect2: {
      width: 365,
      height: 93,
      backgroundColor: "rgba(255,255,255,1)",
      borderRadius: 8,
      marginTop: 25,
      marginLeft: 23
    },
    palestra: {
      color: "rgba(65,117,5,1)",
      fontSize: 18,
      fontFamily: "Montserrat-Medium",
      lineHeight: 17,
      marginTop: 24,
      marginLeft: 24
    },
    icon: {
      color: "rgba(155,155,155,1)",
      fontSize: 15,
      height: 15,
      width: 11
    },
    johnDoe: {
      color: "rgba(155,155,155,1)",
      fontSize: 12,
      fontFamily: "Montserrat-Medium",
      lineHeight: 17,
      marginLeft: 10
    },
    icon2: {
      color: "rgba(155,155,155,1)",
      fontSize: 15,
      height: 15,
      width: 13,
      marginLeft: 47
    },
    johnDoe2: {
      color: "rgba(155,155,155,1)",
      fontSize: 12,
      fontFamily: "Montserrat-Medium",
      lineHeight: 17,
      marginLeft: 7
    },
    iconRow: {
      height: 17,
      flexDirection: "row",
      marginTop: 13,
      marginLeft: 24,
      marginRight: 86
    },
    qrcode: {
      width: 400,
      height: 400,
      marginLeft: 9
    },
  });

export default Perfil;