import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    fontSize: 36,
    marginBottom: 16
  },
  androidButtonText: {
    color: 'blue',
    fontSize: 20
  },
  container: {
    flex: 1,
    backgroundColor: "rgba(248,242,255,1)"
  },
  scrollArea: {
    width: 375,
    height: 359,
    backgroundColor: "#f8f2ff",
    marginTop: 20
  },
  scrollArea_contentContainerStyle: {
    width: 375,
    height: 1795,
    flexDirection: "column",
    alignItems: 'center',
  },
  loremIpsum: {
    color: "rgba(116,23,201,1)",
    fontSize: 18,
    fontFamily: "Montserrat-Bold",
    marginLeft: 1
  },
  group: {
    width: 170,
    height: 48,
    flexDirection: "row",
    marginTop: 14
  },
  image: {
    width: 51,
    height: 48
  },
  image2: {
    width: 51,
    height: 48,
    marginLeft: 8
  },
  image3: {
    width: 51,
    height: 48,
    marginLeft: 9
  },
  
  imageRow: {
    height: 48,
    flexDirection: "row",
    flex: 1
  },
  ultimosConteudos: {
    color: "rgba(116,23,201,1)",
    fontSize: 18,
    fontFamily: "Montserrat-Bold",
    marginTop: 49,
    marginLeft: 1
  },
  rect2: {
    width: 318,
    height: 70,
    backgroundColor: "rgba(255,255,255,1)",
    borderRadius: 10,
    marginTop: 18,
    marginLeft: 1
  },
  icon2: {
    color: "rgba(128,128,128,1)",
    fontSize: 40,
    height: 40,
    width: 37
  },
  sobre: {
    color: "rgba(0,0,0,1)",
    fontSize: 15,
    fontFamily: "Montserrat-Regular"
  },
  sobre2: {
    color: "rgba(142,140,140,1)",
    fontSize: 14,
    fontFamily: "Montserrat-Regular",
    marginTop: 4
  },
  sobreColumn: {
    width: 178,
    marginLeft: 25,
    marginTop: 3,
    marginBottom: 4
  },
  icon2Row: {
    height: 40,
    flexDirection: "row",
    marginTop: 15,
    marginLeft: 19,
    marginRight: 59
  },
  loremIpsumColumn: {
    width: 319,
    marginTop: 13,
    marginLeft: 28
  },
  loremIpsumColumnFiller: {
    flex: 1
  },
  group1: {
    width: 319,
    height: 45,
    marginBottom: 43,
    marginLeft: 28
  },
  rect4: {
    height: 45,
    backgroundColor: "rgba(196,2,2,1)",
    borderRadius: 10,
    flexDirection: "row"
  },
  endWrapperFiller: {
    flex: 1,
    flexDirection: "row"
  },
  ellipse: {
    width: 13,
    height: 13,
    marginRight: 13,
    marginTop: 5
  },
  live: {
    color: "rgba(255,255,255,1)",
    fontSize: 22,
    fontFamily: "Montserrat-Bold"
  },
  ellipseRow: {
    height: 22,
    flexDirection: "row",
    marginRight: 7,
    marginTop: 11
  },
  rect5: {
    height: 128,
    backgroundColor: "#ffcc30",
    marginTop: 0
  },
  materialHeader4: {
    height: 56,
    marginTop: 32
  },
  group3: {
    width: '100%',
    height: 61,
    marginTop: 36,
    marginLeft: 28
  },
  
  bemVindoPhillipe4Stack: {
    width: '100%',
    height: 69
  },
  scrollArea2: {
    width: 375,
    height: 225,
    backgroundColor: "#f8f2ff"
  },
  scrollArea2_contentContainerStyle: {
    width: 1875,
    height: 225,
    flexDirection: "column",
   

  },
  btnmenu: {
    width: 90,
    height: 90
  },
  btnmenu3: {
    width: 90,
    height: 90,
    marginLeft: 18
  },
  group2: {
    width: 90,
    height: 90,
    marginLeft: 17
  },
  rect6: {
    width: 90,
    height: 90,
    backgroundColor: "rgba(255,255,255,1)",
    borderRadius: 5
  },
  icon3: {
    color: "rgba(208,2,27,1)",
    fontSize: 28,
    height: 28,
    width: 25,
    alignSelf: "flex-end",
    marginTop: 19,
    marginRight: 31
  },
  live3: {
    color: "#652c96",
    fontSize: 13,
    fontFamily: "Montserrat-Bold",
    textAlign: "center",
    marginTop: 9
  },
  btnmenuRow: {
    height: 90,
    flexDirection: "row",
    marginTop: 15,
    marginLeft: 29,
    marginRight: 41
  },
  btnmenu2: {
    width: 90,
    height: 90
  },
  btnmenu4: {
    width: 90,
    height: 90,
    marginLeft: 18
  },
  btnmenu2Row: {
    height: 90,
    flexDirection: "row",
    marginTop: 13,
    marginLeft: 29,
    marginRight: 148
  },
  containerCatalogo: {
    width: 313,
    height: 87
  },
  catalogo1: {
    marginTop:30,
    width: 270,
    height: 87
  }
});