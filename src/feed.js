import React from 'react';
import { View, Text, StyleSheet, Button, Platform, TouchableOpacity } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';

import Loginform from "./components/Loginform";
import MaterialHelperTextBox1 from "./components/MaterialHelperTextBox1";
import MaterialSwitch from "./components/MaterialSwitch";
import MaterialButtonViolet from "./components/MaterialButtonViolet";
import MaterialButtonWithShadow from "./components/MaterialButtonWithShadow";

//import { styles } from './styles/styles';

Feed = () => {

  const navigation = useNavigation();
  const route = useRoute();

  let detailResult = route.params;
  return (
    /*<View style={styles.center}>
      <Text style={styles.title}>
        {detailResult ? detailResult.data : 'Navigation Drawer'}
      </Text>
      {
        Platform.select({
          ios:
            <Button
              title='Go to Feed Item'
              onPress={() => navigation.navigate('Detail', { screenName: "My Detail Screen" })}
            />,
          android:
            <TouchableOpacity
              onPress={() => navigation.navigate('Detail', { screenName: "My Detail Screen" })}>
              <Text style={styles.androidButtonText}>Go to FeedItem</Text>
            </TouchableOpacity>
        })
      }

    </View>*/
    <View style={styles.container}>
      <Text style={styles.criar}>Criar</Text>
      <Text style={styles.acesseSuaConta}>Acesse sua conta</Text>
      <Text style={styles.digiteOsDados}>digite os dados</Text>
      <Loginform
        style={styles.materialHelperTextBox1}
      ></Loginform>
      <MaterialHelperTextBox1
        style={styles.materialHelperTextBox2}
      ></MaterialHelperTextBox1>
      <View style={styles.materialSwitch1Row}>
        <MaterialSwitch style={styles.materialSwitch1}></MaterialSwitch>
        <Text style={styles.lembrarMinhaConta1}>lembrar minha conta</Text>
      </View>
      {
        Platform.select({
          ios:
            <Button
              title='Go to Feed Item'
              onPress={() => navigation.navigate('Home', { screenName: "Home" })}
            />,
          android:
            <TouchableOpacity
              onPress={() => navigation.navigate('Home', { screenName: "Home" })}>
              <Text style={styles.androidButtonText}>Go to FeedItem</Text>
            </TouchableOpacity>
        })
      }
      <MaterialButtonWithShadow
        style={styles.materialButtonWithShadow}
      ></MaterialButtonWithShadow>
      <MaterialButtonWithShadow
        text1="Continuar com Google"
        style={styles.materialButtonWithShadow2}
      ></MaterialButtonWithShadow>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  criar: {
    color: "#121212",
    fontSize: 18,
    fontFamily: "Montserrat-Regular",
    //lineHeight: 14,
    marginTop: 55,
    marginLeft: 298,
    fontWeight: '700'
  },
  acesseSuaConta: {
    color: "#121212",
    fontSize: 30,
    fontFamily: "Roboto",
    //lineHeight: 14,
    marginTop: 64,
    marginLeft: 29,
    fontWeight: '700'
  },
  digiteOsDados: {
    color: "#121212",
    fontSize: 18,
    fontFamily: "Montserrat-Regular",
    lineHeight: 14,
    marginTop: 16,
    marginLeft: 32
  },
  materialHelperTextBox1: {
    height: 90,
    marginTop: 138,
    marginLeft: 29,
    marginRight: 27
  },
  materialHelperTextBox2: {
    height: 90,
    marginTop: -186,
    marginLeft: 29,
    marginRight: 27
  },
  materialSwitch1: {
    width: 45,
    height: 23
  },
  lembrarMinhaConta1: {
    color: "#121212",
    fontSize: 16,
    fontFamily: "Montserrat-Regular",
    lineHeight: 14,
    marginLeft: 11,
    marginTop: 4
  },
  materialSwitch1Row: {
    height: 23,
    flexDirection: "row",
    marginTop: 118,
    marginLeft: 31,
    marginRight: 141
  },
  materialButtonViolet1: {
    width: 318,
    height: 36,
    marginTop: 28,
    marginLeft: 27
  },
  materialButtonWithShadow: {
    width: 318,
    height: 36,
    marginTop: 19,
    marginLeft: 27
  },
  materialButtonWithShadow2: {
    width: 318,
    height: 36,
    marginTop: 19,
    marginLeft: 27
  }
});

export default Feed;