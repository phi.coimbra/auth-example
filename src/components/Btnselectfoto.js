import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";

function Btnselectfoto(props) {
  return (
    <View style={[styles.container, props.style]}>
      <TouchableOpacity style={styles.button}>
        <View style={styles.iconRow}>
          <Icon name="camera" style={styles.icon}></Icon>
          <Text style={styles.selecioneUmaFoto}>Selecione uma foto</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  button: {
    width: 236,
    height: 36,
    backgroundColor: "rgba(104,173,26,1)",
    borderRadius: 20,
    flexDirection: "row"
  },
  icon: {
    color: "rgba(255,255,255,1)",
    fontSize: 15,
    height: 15,
    width: 16
  },
  selecioneUmaFoto: {
    color: "rgba(255,255,255,1)",
    fontSize: 15,
    fontFamily: "Montserrat-Regular",
    lineHeight: 17,
    marginLeft: 8
  },
  iconRow: {
    height: 17,
    flexDirection: "row",
    flex: 1,
    marginRight: 44,
    marginLeft: 39,
    marginTop: 10
  }
});

export default Btnselectfoto;
