import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";

function Btnsalvar(props) {
  return (
    <View style={[styles.container, props.style]}>
      <TouchableOpacity style={styles.button}>
        <Text style={styles.salvar}>Salvar</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  button: {
    width: 236,
    height: 36,
    backgroundColor: "rgba(104,173,26,1)",
    borderRadius: 20
  },
  salvar: {
    color: "rgba(255,255,255,1)",
    alignItems: "center",
    fontSize: 15,
    fontFamily: "Montserrat-Regular",
    lineHeight: 17,
    marginTop: 10,
    marginLeft: 98
  }
});

export default Btnsalvar;
