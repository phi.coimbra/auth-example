import React from 'react';

const antidotologo = 'http://www.antidotodesign.com.br/dev/talk/antidoto-logo-preto.JPG'

export default messagesdemo = [
    {
      _id: 1,
      text: 'Depois de assistir a LIVE você pode jogar o quiz clicando no botão que irá aparecer aqui no chat!',
      createdAt: new Date("2020-06-13T00:12:49.126Z"),
      user: {
        _id: 2,
        name: 'Antidoto',
        avatar: antidotologo,
      },
    },
    {
      _id: 2,
      text: 'É possível fazer um planejamento de lives integrado com agenda? ',
      createdAt: new Date('2020-06-13T00:13:49.126Z'),
      user: {
        _id: 1,
        name: 'Usuário',
        avatar: antidotologo,
      },
    },
    {
      _id: 3,
      text: 'É possível sim! E o Antídoto Talk ainda envia push notification de avisos para os cadastrados. ',
      createdAt: new Date('2020-06-13T00:14:49.126Z'),
      user: {
        _id: 2,
        name: 'Antidoto',
        avatar: antidotologo,
      },
    },
    {
      _id: 4,
      text: 'Se for videoaula ao invés de live também tem chat?',
      createdAt: new Date('2020-06-13T00:15:49.126Z'),
      user: {
        _id: 1,
        name: 'Usuário',
        avatar: antidotologo,
      },
    },
    {
      _id: 5,
      text: 'Também tem chat! ',
      createdAt: new Date('2020-06-13T00:16:49.126Z'),
      user: {
        _id: 2,
        name: 'Antidoto',
        avatar: antidotologo,
      },
    },
    {
      _id: 6,
      text: 'Achei bem legal e completo! Como eu faço a contratação? ',
      createdAt: new Date('2020-06-13T00:17:49.126Z'),
      user: {
        _id: 1,
        name: 'Usuário',
        avatar: antidotologo,
      },
    },
    {
      _id: 7,
      text: 'Entre em contato com nossa equipe comercial, nos canais descritos no site que alinhamos os detalhes e valores de investimento.',
      createdAt: new Date('2020-06-13T00:18:49.126Z'),
      user: {
        _id: 2,
        name: 'Antidoto',
        avatar: antidotologo,
      },
    },
    
    {
      _id: 9,
      text: 'Pode ser tanto para um evento virtual de um dia como para planejamentos com períodos mais extensos.',
      createdAt: new Date('2020-06-13T00:20:49.126Z'),
      user: {
        _id: 2,
        name: 'Antidoto',
        avatar: antidotologo,
      },
    },
    {
      _id: '565758',
      text: 'A contratação e por período?',
      createdAt: new Date('2020-06-13T00:19:49.126Z'),
      user: {
        _id: 1,
        name: 'Usuário',
        avatar: antidotologo,
      },
    },
  ]
