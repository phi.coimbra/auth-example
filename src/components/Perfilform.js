import React, { Component, useContext, useReducer, useState, useEffect } from "react";
import { getUniqueId, getSystemName } from 'react-native-device-info'
import { StyleSheet, View, Text, TextInput, Image , Alert, TouchableOpacity, Keyboard} from "react-native";
import { Formik } from 'formik';
import MaterialButtonIn from "./MaterialButtonIn";
import { DB, login, Dispatch, createAccount, setLoggedUser, Users, Auth } from '../helpers/hooks';
import { useNavigation, useRoute } from '@react-navigation/native';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';
import RNFS from 'react-native-fs';
import UUIDGenerator from 'react-native-uuid-generator';
import _ from "lodash";

const options = {
  title: 'Select Avatar',
  
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

function Perfilform(props) {

  const db = useContext(DB)
  const reducer = useContext(Dispatch)
  const clientAuth = useContext(Auth)
  const navigation = useNavigation();
  const [formState, setFormState] = useState({nome:null, sobrenome: null, image:null, id:null})
  const [isKeyboardVisible, setKeyboardVisible] = useState(false);

  //const URL_SERVER = 'http://192.168.1.48:3030/images/'
  const URL_SERVER = 'http://talk.antido.to:3030/images/'

  const uniqueID = getUniqueId();
  const [{user}, dispatch] = reducer;

  useEffect(() => {

    const keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      () => {
        setKeyboardVisible(true); // or some other action
      }
    );
    const keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      () => {
        setKeyboardVisible(false); // or some other action
      }
    );

    clientAuth.service('uploads').on('created', image => {
      console.log('New image sended', image.params.email)
      if(props.doc.email === image.params.email){
        setFormState(prev => ({ 
          ...prev,
          image:URL_SERVER + image.id
        }))
      }
    });

    //console.log('thedoc:', props)
    db['users'].findOne().where('email').eq(props.doc.email).exec().then(item => {
      //console.log('get perfil', item)
      if(item){
          setFormState(
            {nome:item.nome, sobrenome:item.sobrenome, image:item.image}
          )
      }else{
        UUIDGenerator.getRandomUUID().then((uuid) => {
          //console.log(uuid);
          setFormState(prev => ({ 
            nome:'', 
            sobrenome:'',
            id:uuid,
            image:URL_SERVER + 'avatar.png'
          }))
        });
      }
    });

    return () => {
      keyboardDidHideListener.remove();
      keyboardDidShowListener.remove();
    };

  },[])

  showPicker = () => {

    setFormState(prev => ({ 
      ...prev,
      image:null
      }))

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);
    
      if (response.didCancel) {
        setFormState(prev => ({ 
          ...prev,
          image:URL_SERVER + 'avatar.png'
          }))
        console.log('User cancelled image picker');
      } else if (response.error) {
        setFormState(prev => ({ 
          ...prev,
          image:URL_SERVER + 'avatar.png'
          }))
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
       // const source = { uri: response.uri };
    
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

       /* setFormState(prev => ({ 
          ...prev,
            image:'data:image/'+response.uri.substr(response.lastIndexOf('.'), 4)+';base64,' + response.data
          }))*/
    
          //console.log('photo grabbed', 'data:image/'+response.uri.substr(response.uri.lastIndexOf('.')+1, 4)+';base64')
        //console.log('data:image/jpeg;base64,' + response.data)
        resizeImage(response.uri)
      }
    });
  }


  resizeImage = async (path) => {
    console.log('resize')
    
    const resizedImageUrl = await ImageResizer.createResizedImage(path, 400, 400, 'PNG', 80, 0, RNFS.DocumentDirectoryPath);

    console.log('Resized', resizedImageUrl.path)

    const base64 = await RNFS.readFile(resizedImageUrl.path, 'base64');

    //console.log('base64')

    //console.log('returned = ', 'data:image/png;base64,'+ base64)

    

// Call the `messages` service
    clientAuth.service('uploads').create({
      uri: 'data:image/png;base64,' + base64,
      params:{email:props.doc.email}
    }); 

    /*setFormState(prev => ({ 
      ...prev,
        image:'data:image/png;base64,' + base64
      }))*/

      

  }

  salvadados = async (values) => {

    
    db['users'].findOne().where('email').eq(props.doc.email).exec().then(item => {

      //console.log('temUSER:', item, props.doc.email)
      const deviceID = getUniqueId()

      if(!item){

        console.log('try save:', item, props.doc)

        props.doc.nome = values.nome;
        props.doc.sobrenome = values.sobrenome;
        props.doc.device = deviceID;
        props.doc.id = formState.id;
        props.doc.image = formState.image;
        props.doc.logged = true;

        props.doc.save();

        console.log('saved', values.nome, values.sobrenome, deviceID, formState.id, formState.image)
      }else{

        //const myData = _.clone(item)
        

        //console.log('1   myData:', myData)

        salva(values, item)
        console.log('saved', 2)

      }
    })
    /*gun.get('devices').get(uniqueID).get('perfil').put({
        nome:values.nome,
        sobrenome:values.sobrenome,
        image:formState.avatar
    });*/
    dispatch({type:'login'})
    //console.log('islogged:', logged)
    //
  }

  salva = async (values, item) => {

    //var merged = _.merge({}, item, {id: item.id, nome:values.nome, sobrenome:values.sobrenome, image:formState.image});

    var merged = JSON.parse(JSON.stringify(item))

    merged.id = item.id
    merged.nome = values.nome,
    merged.sobrenome = values.sobrenome,
    merged.image = formState.image

    //console.log('2  myData:', merged)

   // console.log('3  myData:', myData)

    const upsert = await db['users'].upsert(merged);

    console.log('upsert:', upsert)
  }


  salvaperfil = (values) => {
    //console.log('data', values )
    if(values.nome === '' || values.sobrenome === '' ){
      Alert.alert('Dados não preenchidos', 'Complete corretamente o cadastro');
      return
    }

    salvadados(values)

    //verifyLogin(values)
   /* gun.get('antidoto').get('usuarios').map().once(function(data, key){
      console.log('item', data.email, key )
      console.log('nome', data.nome )
   })*/

   /*gun.get('antidoto').get('usuarios').get('phillipe@antidotodesign.com.br').get('name').once(function(data, key){
    console.log('item', data )

  })*/
    //navigation.navigate('Home', { screenName: "Home" })
  }

  handleChange2 = (e,r) => {
    //console.log('change', e, r)
    const data = e.data
      setFormState(prev => ({ 
      ...prev,
        data  
      }))
  }


  handleBlur = () => {
    
  }

  getTextStyle = () => {
    if(!isKeyboardVisible) {
     return {
      width: 200,  height: 200, alignContent:'center'
     }
    } else {
      return {
        width: 50,  height: 50, alignContent:'center'
      }
    }
   }

  return (
    <React.Fragment>
    {formState.nome !== null?<Formik
      initialValues={formState}
      onSubmit={salvaperfil}
    >
    {({ handleChange, handleBlur, submitForm, values }) => (
      
    <React.Fragment>
      <View style={[getTextStyle()]}>
        {formState.image && (
        <TouchableOpacity 
          onPress={() => showPicker()}
          style={[getTextStyle()]}
              >
            <Image
              source={{uri:formState.image}}
              style={styles.infoimage}
              resizeMode="cover"
            ></Image>
        </TouchableOpacity>
        )}
      </View>
      <View style={[styles.container, styles.tamanho]}>
        <Text style={styles.label}>Nome</Text>
        <TextInput
          placeholder="insira seu nome"
          onChangeText={handleChange('nome')}
          onBlur={handleBlur('nome')}
          value={values.nome}
          style={styles.inputStyle}
          
        />
        
      </View>
      <View style={[styles.container, styles.tamanho]}>
        <Text style={styles.label}>Sobrenome</Text>
          <TextInput
            placeholder="insira seu sobrenome"
            onChangeText={handleChange('sobrenome')}
            onBlur={handleBlur('sobrenome')}
            value={values.sobrenome}
            style={styles.inputStyle}
            
          />
      </View>
      
      <View style={[styles.botoes, styles.tamanho]}>

        <MaterialButtonIn
            text1='Salvar'
            onPress={submitForm}
            style={styles.materialButtonIn}
          ></MaterialButtonIn>
      </View>

    </React.Fragment>

    )}
  </Formik>
  :
  <Text>Carregando...</Text>
  }
  </React.Fragment> 
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "transparent"
  },
  label: {
    color: "#000",
    opacity: 0.6,
    paddingTop: 6,
    fontSize: 12,
    fontFamily: "Montserrat-Regular",
    textAlign: "left"
  },
  inputStyle: {
    //width: 375,
    
    flex: 1,
    color: "#000",
    alignSelf: "stretch",
    paddingTop: 8,
    paddingBottom: 8,
    borderColor: "#D9D5DC",
    borderBottomWidth: 1,
    fontSize: 16,
    fontFamily: "Montserrat-Regular",
    //lineHeight: 20
  },
  helper: {
    color: "#000",
    opacity: 0.6,
    paddingTop: 8,
    fontSize: 12,
    fontFamily: "Montserrat-Regular",
    textAlign: "left"
  },
  tamanho:{
    height: 60,
    
    alignSelf: "stretch",
  },
  materialButtonIn:{
    alignItems: 'stretch',
    marginTop:20,
    height: 40,
    

    //minWidth:150
  },
  botoes:{

    alignItems: 'stretch',

  },
  materialButtonShare2: {
    width: 56,
    height: 56,
    marginRight:10,
    backgroundColor: "rgba(239,44,67,1)"
  },
  materialButtonShare: {
    width: 56,
    height: 56,
  },
  share:{
    flexDirection: "row",
    alignItems: 'flex-start',
    justifyContent: 'center',
    marginTop:20
  },
  infoimage: {
    flex:1,
    height: undefined, 
    width: undefined,
    alignSelf: 'stretch',
    borderRadius:300,
   // borderWidth: 1
   
    //marginLeft: 9
  }
});

export default Perfilform;

