import React, { Component, useContext } from "react";
import { $Gun } from '../../UserContext';
import { StyleSheet, TouchableOpacity, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";


function MaterialButtonIn(props) {

  /*const gun = useContext($Gun)

  gun.get('antidoto').get('usuarios').map().on((item) => {
     console.log('nome', item.nome )
  })*/

  return (
    <TouchableOpacity 
    onPress={props.onPress}
    style={[styles.container, props.style]}>
     <Text style={styles.criar}>{props.text1}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#000",
    alignItems: "center",
    justifyContent: "center",
    elevation: 2,
    minWidth: 40,
    minHeight: 40,
    borderRadius: 15,
    shadowOffset: {
      height: 2,
      width: 0
    },
    shadowColor: "#111",
    shadowOpacity: 0.2,
    shadowRadius: 1.2
  },
  icon: {
    color: "#fff",
    fontFamily: "Montserrat-Regular",
    fontSize: 24,
    alignSelf: "center"
  },
  criar: {
    color: "#fff",
    fontSize: 18,
    fontFamily: "Montserrat-Regular",
    //lineHeight: 14,
    alignSelf: "center",
    fontWeight: '700'
  },
});

export default MaterialButtonIn;
