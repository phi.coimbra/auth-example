import React, { Component, useContext, useReducer } from "react";
import { StyleSheet, View, Text, TextInput, KeyboardAvoidingView , Alert, Keyboard, ScrollView} from "react-native";
import { Formik } from 'formik';
import MaterialButtonIn from "./MaterialButtonIn";
import { DB, login, Dispatch, createAccount, setLoggedUser, Users, Auth } from '../helpers/hooks';
import { useNavigation, useRoute } from '@react-navigation/native';
import MaterialButtonShare from "../components/MaterialButtonShare";

function Signupform(props) {

  const db = useContext(DB)
  const reducer = useContext(Dispatch)
  const usersService = useContext(Users)
  const clientAuth = useContext(Auth)
  const navigation = useNavigation();

  const [{logged, cliente}, dispatch] = reducer;
  
  verifyExistLogin = async (values) => {
   
    db['evento'].findOne().where('nome').eq(values.idevento.toLowerCase()).exec().then(doc => 
      
      {
       // console.log('evento:', values.idevento.toLowerCase())
        if(doc){
          criaUsuario(values)
        }else{
          Alert.alert('Evento não encontrado', 'ID de evento inexistente');
        }
      });

   /*   gun.get(values.idevento).once(function(data, key){
        //console.log('data ok', data, cliente)
        //Alert.alert('Evento não encontrado', 'ID de evento inexistente');
        if(data){

          criaUsuario(values)
         
          
        }else{
          Alert.alert('Evento não encontrado', 'ID de evento inexistente');
        }
        
      })*/

    //console.log('islogged:', logged)
    //
  }

  criaUsuario = async (values) => {
    let created = await createAccount(clientAuth, values)
    console.log('created:', created)

    if(created){
      const doc = db['users'].newDocument({
        email:values.email,
        evento:values.idevento.toLowerCase()
      });
      console.log('doc', doc)

      let logginin = await login(clientAuth, values)
      console.log('logginin:', logginin)
      dispatch({type:'userdata', payload:{email:logginin.user.email}})
      navigation.navigate('Perfiledit', { screenName: "Perfiledit", doc:doc })
      //setLoggedUser(db, values.idevento.toLowerCase())
      dispatch({type:'cliente', payload:values.idevento.toLowerCase()})
    }else{
      Alert.alert('Cadastro existente', 'Já existe um cadastro com esse e-mail');
    }

    
  }

  verifySignup = (values) => {
    if(values.email === '' || values.senha === '' || values.confsenha === ''){
      Alert.alert('Erro no cadastro', 'Preencha corretamente os dados');
      return
    }

    if(values.senha !== values.confsenha){
      Alert.alert('Erro no cadastro', 'As senhas precisam ser iguais');
      return
    }

    verifyExistLogin(values)

    //verifyLogin(values)
   /* gun.get('antidoto').get('usuarios').map().once(function(data, key){
      console.log('item', data.email, key )
      console.log('nome', data.nome )
   })*/

   /*gun.get('antidoto').get('usuarios').get('phillipe@antidotodesign.com.br').get('name').once(function(data, key){
    console.log('item', data )

  })*/
    //navigation.navigate('Home', { screenName: "Home" })
  }

  handleChange = () => {

  }


  handleBlur = () => {
    
  }

  function validatePassword(value) {
    let error;
    if (value === 'admin') {
      error = 'Nice try!';
    }
    return error;
  }

  return (
   /* <View style={[styles.container, props.style]}>
      <Text style={styles.label}>senha</Text>
      <TextInput
        placeholder="insira sua senha"
        secureTextEntry={true}
        style={styles.inputStyle}
      ></TextInput>
      
    </View>*/
<ScrollView style={{width:'100%'}}>
    <Formik
    initialValues={{ email: '', senha: '', confsenha:'', idevento:'' }}
    onSubmit={verifySignup}
  >
    {({ errors, touched, handleChange, handleBlur, submitForm, values }) => (
      
    <React.Fragment>
      <View style={[styles.container, styles.tamanho]}>
        <Text style={styles.label}>Evento</Text>
        <TextInput
          placeholder="insira o ID do evento"
          onChangeText={handleChange('idevento')}
          onBlur={handleBlur('idevento')}
          value={values.idevento.toLowerCase()}
          style={styles.inputStyle}
          autoCapitalize = 'none'
          autoCorrect={false}
        />
        
      </View>
      <View style={[styles.container, styles.tamanho]}>
        <Text style={styles.label}>Login</Text>
          <TextInput
            placeholder="insira seu email"
            onChangeText={handleChange('email')}
            onBlur={handleBlur('email')}
            value={values.email}
            style={styles.inputStyle}
            autoCapitalize = 'none'
            autoCorrect={false}
          />
      </View>
      <View style={[styles.container, styles.tamanho]}>
        <Text style={styles.label}>Senha</Text>
        <TextInput
          placeholder="insira sua senha"
          onChangeText={handleChange('senha')}
          onBlur={handleBlur('senha')}
          value={values.senha}
          secureTextEntry={true}
          style={styles.inputStyle}
          blurOnSubmit={false}
          textContentType={'oneTimeCode'}
          onSubmitEditing={()=> Keyboard.dismiss()}
        />
        {errors.senha && touched.senha && <div>{errors.senha}</div>}
        
      </View>
      <View style={[styles.container, styles.tamanho]}>
        <Text style={styles.label}>Confirmação de senha</Text>
        <TextInput
          placeholder="confirme sua senha"
          onChangeText={handleChange('confsenha')}
          onBlur={handleBlur('confsenha')}
          value={values.confsenha}
          secureTextEntry={true}
          style={styles.inputStyle}
          blurOnSubmit={false}
          textContentType={'oneTimeCode'}
          onSubmitEditing={()=> Keyboard.dismiss()}
        />
        
      </View>
      

      <View style={[styles.botoes, styles.tamanho]}>

        <MaterialButtonIn
            text1='Cadastrar'
            onPress={submitForm}
            style={styles.materialButtonIn}
          ></MaterialButtonIn>
      </View>
{/*
      <View style={styles.share}>
        <MaterialButtonShare
          icon1Name="google"
          style={styles.materialButtonShare2}
        ></MaterialButtonShare>
        <MaterialButtonShare
          icon1Name="facebook"
          style={styles.materialButtonShare}
        ></MaterialButtonShare>
      </View>
*/}

    </React.Fragment>

    )}
  </Formik>
  </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "transparent"
  },
  label: {
    color: "#000",
    opacity: 0.6,
    paddingTop: 6,
    fontSize: 12,
    fontFamily: "Montserrat-Regular",
    textAlign: "left"
  },
  inputStyle: {
    //width: 375,
    
    flex: 1,
    color: "#000",
    alignSelf: "stretch",
    paddingTop: 8,
    paddingBottom: 8,
    borderColor: "#D9D5DC",
    borderBottomWidth: 1,
    fontSize: 16,
    fontFamily: "Montserrat-Regular",
    //lineHeight: 20
  },
  helper: {
    color: "#000",
    opacity: 0.6,
    paddingTop: 8,
    fontSize: 12,
    fontFamily: "Montserrat-Regular",
    textAlign: "left"
  },
  tamanho:{
    height: 60,
    marginTop: 10,
    alignSelf: "stretch",
  },
  materialButtonIn:{
    alignItems: 'stretch',
    marginTop:20,
    height: 40,
    

    //minWidth:150
  },
  botoes:{

    alignItems: 'stretch',

  },
  materialButtonShare2: {
    width: 56,
    height: 56,
    marginRight:10,
    backgroundColor: "rgba(239,44,67,1)"
  },
  materialButtonShare: {
    width: 56,
    height: 56,
  },
  share:{
    flexDirection: "row",
    alignItems: 'flex-start',
    justifyContent: 'center',
    marginTop:20
  },
});

export default Signupform;
