import React, { Component, useContext, useState, useEffect } from "react";
import { StyleSheet, View, Text, TextInput, Keyboard , Alert, TouchableOpacity, Image, ScrollView} from "react-native";
import { Formik } from 'formik';
import MaterialButtonIn from "./MaterialButtonIn";
import { DB, login, Dispatch, setLoggedUser, Auth, isLogged, userData } from '../helpers/hooks';
import { useNavigation, useRoute } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import MaterialSwitch from "../components/MaterialSwitch";
import MaterialButtonShare from "../components/MaterialButtonShare";

function Loginform(props) {

  const db = useContext(DB)
  const reducer = useContext(Dispatch)
  const clientAuth = useContext(Auth)
  const navigation = useNavigation();
  const [userdata, setUserdata] = userData()
  const [checked, setChecked] = useState(false)
  const [isKeyboardVisible, setKeyboardVisible] = useState(false);

  const [{logged, cliente}, dispatch] = reducer;
  //const [{logged}, dispatch] = useReducer(logginReducer, {})
  
  
   useEffect(() => {

    const keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      () => {
        setKeyboardVisible(true); // or some other action
      }
    );
    const keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      () => {
        setKeyboardVisible(false); // or some other action
      }
    );

    return () => {
      keyboardDidHideListener.remove();
      keyboardDidShowListener.remove();
    };
   },[])

  /*gun.get('antidoto').get('usuarios').map().on((item) => {
     console.log('nome', item.nome )
  })*/

  //console.log('gun:', gun)

  verifyLogin = async (values) => {
   console.log('tenta login')
   let logged = await login(clientAuth, values, db)

   // console.log('passou', db.antidoto)
  // db['users'].findOne().where('email').eq('phi.coimbra@gmail.com').exec().then(doc => console.log('resposta: ', doc));

    if(!logged){
      Alert.alert('Erro no login', 'Usuário ou senha incorreta');
      /*let created = await createUser(gun, values)

      if(created){
        dispatch({type:'login'})
      }*/
    }else{

      let evento = values.idevento.toLowerCase()
 
      db['evento'].findOne().where('nome').eq(evento).exec().then(doc => {
        
        console.log('resposta: ', doc)
       
        if(doc){
          dispatch({type:'userdata', payload:{email:logged.user.email}})
          dispatch({type:'cliente', payload:evento})
          logPut(db, values)
        }else{
          Alert.alert('Evento não encontrado', 'ID de evento inexistente');
        }

      });

     /* gun.get(values.idevento.toLowerCase()).once(function(data, key){
        console.log('data ok', data, cliente.toLowerCase())
        //Alert.alert('Evento não encontrado', 'ID de evento inexistente');
        if(data){
          dispatch({type:'cliente', payload:values.idevento.toLowerCase()})
          logPut(gun, values.idevento.toLowerCase())
          
        }else{
          Alert.alert('Evento não encontrado', 'ID de evento inexistente');
        }
        
      })*/
      //console.log(values)
    }

    //console.log('islogged:', logged)
    //
  }

  logPut = async (db, values) => {
    let loggedin = await isLogged(db);

   // console.log('logged:', loggedin)

    if(!loggedin){
      console.log('setlogged');
      setLoggedUser(db, values.email)
    }

    //setUserdata(values)
    /*db['users'].findOne().where('email').eq(values.email).exec().then(doc => {
      dispatch({type:'userdata', payload:doc})
    })*/

    dispatch({type:'login'})
   // Alert.alert('Logged IN', 'Logado');
  }

  verify = (values) => {
    if(values.email === '' || values.senha === '' || values.idevento ===''){
      Alert.alert('Erro no login', 'Preencha corretamente os campos');
      return
    }

    verifyLogin(values)

    //verifyLogin(values)
   /* gun.get('antidoto').get('usuarios').map().once(function(data, key){
      console.log('item', data.email, key )
      console.log('nome', data.nome )
   })*/

   /*gun.get('antidoto').get('usuarios').get('phillipe@antidotodesign.com.br').get('name').once(function(data, key){
    console.log('item', data )

  })*/
    //navigation.navigate('Home', { screenName: "Home" })
  }

  handleChange = () => {

  }


  handleBlur = () => {
    
  }

  onPress = () => {
  
      setChecked(!checked);

  }

  getDynamicStyle =() => {
    if(!isKeyboardVisible){
      
      return (
        {flex:1, height: 40,  width: undefined, alignSelf: 'stretch', maxHeight:100, alignContent:'center', marginTop:20}
      )
    }else{
      return (
        {flex:1, height: 20,  width: 40, alignSelf: 'center', maxHeight:50, alignContent:'center'}
      )
    }
  }

  return (
   /* <View style={[styles.container, props.style]}>
      <Text style={styles.label}>senha</Text>
      <TextInput
        placeholder="insira sua senha"
        secureTextEntry={true}
        style={styles.inputStyle}
      ></TextInput>
      
    </View>*/
<ScrollView style={{width:'100%'}}
    >
    <Formik
    initialValues={{ email: '', senha: '', idevento: '' }}
    onSubmit={verify}
  >
    {({ handleChange, handleBlur, submitForm, values }) => (

      
      <React.Fragment>
        <View style={[styles.container, styles.tamanho]}>
        <Text style={styles.label}>Evento</Text>
          <TextInput
            placeholder="insira o ID do evento"
            onChangeText={handleChange('idevento')}
            onBlur={handleBlur('idevento')}
            value={values.idevento}
            style={styles.inputStyle}
            autoCapitalize = 'none'
            autoCorrect={false}
          />
        </View>
        <View style={[styles.container, styles.tamanho]}>
          <Text style={styles.label}>Login</Text>
            <TextInput
              placeholder="insira seu email"
              onChangeText={handleChange('email')}
              onBlur={handleBlur('email')}
              value={values.email}
              style={styles.inputStyle}
              autoCapitalize = 'none'
              autoCorrect={false}
            />
        </View>
        <View style={[styles.container, styles.tamanho]}>
          <Text style={styles.label}>Senha</Text>
          <TextInput
            placeholder="insira sua senha"
            onChangeText={handleChange('senha')}
            onBlur={handleBlur('senha')}
            value={values.senha}
            secureTextEntry={!checked}
            style={styles.inputStyle}
            autoCapitalize = 'none'
            textContentType={'password'}
            //textContentType={'oneTimeCode'}
          />
          
          
        </View>
        <View style={{alignSelf:'flex-start'}}>
          <View style={{flexDirection:'row', alignItems:'center', alignContent:'flex-start', height:60}}>
            <Icon name={checked ? "checkbox-marked" : "checkbox-blank-outline"} onPress={onPress} size={30}/>
            <Text style={{fontSize:16, marginHorizontal:5}}>Mostrar senha</Text>
          </View>
        </View>

     {/* <View style={styles.materialSwitch1Row}>
          <MaterialSwitch style={styles.materialSwitch1}></MaterialSwitch>
        <Text style={styles.lembrarMinhaConta1}>lembrar minha conta</Text>
      </View>
    */}

      <View style={[styles.botoes, styles.tamanho]}>

        <MaterialButtonIn
            text1='Login'
            onPress={submitForm}
            style={styles.materialButtonIn}
          ></MaterialButtonIn>
        </View>

        <TouchableOpacity onPress={() => navigation.navigate('Recover', { screenName: "Recover" })}>
          <Text style={styles.signuptext}>Esqueci a senha. <Text style={{color: '#ffcc30'}}>Clique aqui</Text>.</Text>
        </TouchableOpacity>

        <Image
          source={require("../assets/images/logo_antidoto.png")}
          style={getDynamicStyle()}
          resizeMode="contain"
        ></Image>
        
        {/*

        <View style={styles.share}>
          <MaterialButtonShare
            onPress={() => navigation.navigate('Perfiledit', { screenName: "Perfiledit" })}  
            icon1Name="google"
            style={styles.materialButtonShare2}
          ></MaterialButtonShare>
          <MaterialButtonShare
            onPress={() => dispatch({type:'login'})}
            icon1Name="facebook"
            style={styles.materialButtonShare}
          ></MaterialButtonShare>
        </View>

        */}
       

      </React.Fragment>
      
    )}
  </Formik>
  </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "transparent"
  },
  label: {
    color: "#000",
    opacity: 0.6,
    paddingTop: 6,
    fontSize: 12,
    fontFamily: "roboto-regular",
    textAlign: "left"
  },
  inputStyle: {
    //width: 375,
    
    flex: 1,
    color: "#000",
    alignSelf: "stretch",
    paddingTop: 8,
    paddingBottom: 8,
    borderColor: "#D9D5DC",
    borderBottomWidth: 1,
    fontSize: 16,
    fontFamily: "Montserrat-Regular"
    //lineHeight: 20
  },
  tamanho:{
    height: 60,
    marginTop: 10,
    alignSelf: "stretch",
    
  },
  materialButtonIn:{
    alignItems: 'stretch',
    //marginTop:20,
    height: 20

    //minWidth:150
  },
  botoes:{

    alignItems: 'stretch',

  },
  signuptext: {

    color: "#121212",
    fontSize: 18,
    fontFamily: "Montserrat-Regular",
    lineHeight: 20,
    textAlign: "center"
  },
  materialSwitch1Row: {
    height: 23,
    flexDirection: "row",
    marginTop: 10,
    alignSelf:'flex-start'
   
    
  },
  materialSwitch1: {
    width: 45,
    height: 23
  },
  lembrarMinhaConta1: {
    color: "#121212",
    fontSize: 16,
    fontFamily: "Montserrat-Regular",
    lineHeight: 16,
    marginLeft: 11,
    marginTop: 9
  },
  materialButtonShare: {
    width: 56,
    height: 56,
  },
  materialButtonShare2: {
    width: 56,
    height: 56,
    marginRight:10,
    backgroundColor: "rgba(239,44,67,1)"
  },
  share:{
    flexDirection: "row",
    alignItems: 'flex-start',
    justifyContent: 'center',
    marginTop:20
  },
  infoimage: {
    flex:1,
    height: undefined, 
    width: undefined,
    alignSelf: 'stretch',
    maxHeight:50,
    alignContent:'center'
  },
});

export default Loginform;
