import React, { Component, useContext, useReducer } from "react";
import { StyleSheet, View, Text, TextInput, Button , Alert} from "react-native";
import { Formik } from 'formik';
import MaterialButtonIn from "./MaterialButtonIn";
import { Gun, findUser, Dispatch, createUser } from '../helpers/hooks';
import { useNavigation, useRoute } from '@react-navigation/native';
import MaterialButtonShare from "./MaterialButtonShare";

function Recoverform(props) {

  const gun = useContext(Gun)
  const dispatch = useContext(Dispatch)
  const navigation = useNavigation();
  
  verifyLogin = async (values) => {
   
    let logged = await findUser(gun, values)

    if(!logged){
      let created = await createUser(gun, values)
      console.log('created:', created)

      if(created){
        dispatch({type:'login'})
      }
    }else{
      //dispatch({type:'login'})
      Alert.alert('Cadastro existente', 'Já existe um cadastro com esse e-mail');
    }

    //console.log('islogged:', logged)
    //
  }

  verify = (values) => {
    if(values.email === '' || values.senha === '' || values.confsenha === ''){
      Alert.alert('Erro no cadastro', 'Preencha corretamente os dados');
      return
    }

    if(values.senha !== values.confsenha){
      Alert.alert('Erro no cadastro', 'As senhas precisam ser iguais');
      return
    }

    verifyLogin(values)

    //verifyLogin(values)
   /* gun.get('antidoto').get('usuarios').map().once(function(data, key){
      console.log('item', data.email, key )
      console.log('nome', data.nome )
   })*/

   /*gun.get('antidoto').get('usuarios').get('phillipe@antidotodesign.com.br').get('name').once(function(data, key){
    console.log('item', data )

  })*/
    //navigation.navigate('Home', { screenName: "Home" })
  }

  handleChange = () => {

  }


  handleBlur = () => {
    
  }

  return (
   /* <View style={[styles.container, props.style]}>
      <Text style={styles.label}>senha</Text>
      <TextInput
        placeholder="insira sua senha"
        secureTextEntry={true}
        style={styles.inputStyle}
      ></TextInput>
      
    </View>*/

    <Formik
    initialValues={{ email: '', senha: '', confsenha:'' }}
    onSubmit={verify}
  >
    {({ handleChange, handleBlur, submitForm, values }) => (
      
    <React.Fragment>
      <View style={[styles.container, styles.tamanho]}>
        <Text style={styles.label}>login</Text>
          <TextInput
            placeholder="insira seu email"
            onChangeText={handleChange('email')}
            onBlur={handleBlur('email')}
            value={values.email}
            style={styles.inputStyle}
          />
      </View>
      
      <View style={[styles.botoes, styles.tamanho]}>

        <MaterialButtonIn
            text1='Recuperar'
            onPress={submitForm}
            style={styles.materialButtonIn}
          ></MaterialButtonIn>
      </View>

      

    </React.Fragment>

    )}
  </Formik>

  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "transparent"
  },
  label: {
    color: "#000",
    opacity: 0.6,
    paddingTop: 6,
    fontSize: 12,
    fontFamily: "Montserrat-Regular",
    textAlign: "left"
  },
  inputStyle: {
    //width: 375,
    
    flex: 1,
    color: "#000",
    alignSelf: "stretch",
    paddingTop: 8,
    paddingBottom: 8,
    borderColor: "#D9D5DC",
    borderBottomWidth: 1,
    fontSize: 16,
    fontFamily: "Montserrat-Regular",
    //lineHeight: 20
  },
  helper: {
    color: "#000",
    opacity: 0.6,
    paddingTop: 8,
    fontSize: 12,
    fontFamily: "Montserrat-Regular",
    textAlign: "left"
  },
  tamanho:{
    height: 60,
    marginTop: 10,
    alignSelf: "stretch",
  },
  materialButtonIn:{
    alignItems: 'stretch',
    marginTop:20,
    height: 40,
    

    //minWidth:150
  },
  botoes:{

    alignItems: 'stretch',

  },
  materialButtonShare2: {
    width: 56,
    height: 56,
    marginRight:10,
    backgroundColor: "rgba(239,44,67,1)"
  },
  materialButtonShare: {
    width: 56,
    height: 56,
  },
  share:{
    flexDirection: "row",
    alignItems: 'flex-start',
    justifyContent: 'center',
    marginTop:20
  },
});

export default Recoverform;
