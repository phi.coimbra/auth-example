import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";

function MaterialHeader4(props) {
  return (
    <View style={[styles.container, props.style]}>
      <View style={styles.leftIconButtonRow}>
        <TouchableOpacity /* Conditional navigation not supported at the moment */
          style={styles.leftIconButton}
        >
          <FontAwesomeIcon
            name="align-center"
            style={styles.leftIcon2}
          ></FontAwesomeIcon>
        </TouchableOpacity>
        <View style={styles.textWrapper}>
          <Text numberOfLines={1} style={styles.title}>
            {props.text1 || "Conteúdos"}
          </Text>
        </View>
      </View>
      <View style={styles.leftIconButtonRowFiller}></View>
      <View style={styles.rightIconsWrapper}>
        <TouchableOpacity style={styles.iconButton}>
          <FontAwesomeIcon
            name="bell"
            style={styles.rightIcon1}
          ></FontAwesomeIcon>
        </TouchableOpacity>
        <TouchableOpacity style={styles.iconButton2}>
          <FontAwesomeIcon
            name="user"
            style={styles.rightIcon2}
          ></FontAwesomeIcon>
        </TouchableOpacity>
        <TouchableOpacity style={styles.iconButton3}>
          <FontAwesomeIcon
            name="gear"
            style={styles.rightIcon3}
          ></FontAwesomeIcon>
        </TouchableOpacity>
        <TouchableOpacity style={styles.iconButton4}></TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#652c96",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    padding: 4,
    elevation: 3,
    shadowOffset: {
      height: 2,
      width: 0
    },
    shadowColor: "#111",
    shadowOpacity: 0.2,
    shadowRadius: 1.2
  },
  leftIconButton: {
    padding: 11
  },
  leftIcon2: {
    backgroundColor: "transparent",
    color: "#FFFFFF",
    fontFamily: "Montserrat-Regular",
    fontSize: 24
  },
  textWrapper: {
    marginLeft: 21,
    marginTop: 14
  },
  title: {
    backgroundColor: "transparent",
    color: "#FFFFFF",
    fontSize: 18,
    fontFamily: "Montserrat-Bold",
    lineHeight: 18
  },
  leftIconButtonRow: {
    height: 46,
    flexDirection: "row",
    marginLeft: 5,
    marginTop: 5
  },
  leftIconButtonRowFiller: {
    flex: 1,
    flexDirection: "row"
  },
  rightIconsWrapper: {
    flexDirection: "row",
    alignItems: "center",
    marginRight: 5,
    marginTop: 5
  },
  iconButton: {
    padding: 11
  },
  rightIcon1: {
    backgroundColor: "transparent",
    color: "#FFFFFF",
    fontFamily: "Montserrat-Regular",
    fontSize: 24
  },
  iconButton2: {
    padding: 11
  },
  rightIcon2: {
    backgroundColor: "transparent",
    color: "#FFFFFF",
    fontFamily: "Montserrat-Regular",
    fontSize: 24
  },
  iconButton3: {
    padding: 11
  },
  rightIcon3: {
    backgroundColor: "transparent",
    color: "#FFFFFF",
    fontFamily: "Montserrat-Regular",
    fontSize: 24
  },
  iconButton4: {
    padding: 11
  }
});

export default MaterialHeader4;
