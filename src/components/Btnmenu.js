import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";

function Btnmenu(props) {
  return (
    <View style={[styles.container, props.style]}>
      <View style={styles.rect6}>
        <Icon
          name={props.icon1Name || "calendar-o"}
          style={styles.icon3}
        ></Icon>
        <Text style={styles.agenda}>{props.text1 || "Agenda"}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  rect6: {
    width: 90,
    height: 90,
    backgroundColor: "rgba(255,255,255,1)",
    borderRadius: 5
  },
  icon3: {
    color: "rgba(94,194,116,1)",
    fontSize: 26,
    height: 26,
    width: 24,
    marginTop: 18,
    marginLeft: 30
  },
  agenda: {
    color: "#652c96",
    fontSize: 12,
    fontFamily: "Montserrat-Medium",
    textAlign: "center",
    marginTop: 13
  }
});

export default Btnmenu;
