import React, { useState, useEffect, useRef } from 'react'
import {View, StyleSheet, Text, TouchableOpacity, Dimensions} from 'react-native'
import Animated, {interpolate, SpringUtils, useCode} from 'react-native-reanimated';
import { withSpringTransition, withTimingTransition } from 'react-native-redash'


BtnQuiz = ({id, selected, right, resposta, responde, answered}) => {
    const {set, cond, eq, add, spring, startClock, stopClock, clockRunning, sub, defined, Value, Clock, event } = Animated;
    const [op, setOp] = useState(0)
    const {width, height} = Dimensions.get('window')
    const started = useRef(new Value(0));
    const timming = useRef(new Value(0));
    const timmingAnimation = withTimingTransition(op, { duration: 1000 })
    const timmingAnimationQuick = withTimingTransition(op, { duration: 100 })
    const firstAnimation = withSpringTransition(op, {
        ...SpringUtils.makeDefaultConfig(),
        overshootClamping:true,
        damping:new Value(20),
        mass:new Value(2),
        stiffness:new Value(10)
    })
    
    
    const opacity = interpolate(op, {
        inputRange:[0, .5],
        outputRange:[0, .5]
    })

    useEffect(() =>{
       setOp(1)
    }, [])

    function responder(){
        responde(id)
    }

    function getTrans(){
        if(!answered){
            return {transform:[{translateX:Animated.interpolate(firstAnimation, {
                inputRange: [0, .5, 1],
                outputRange: [-width, 0, 0]})}]}
        }else if(answered && !selected){
            return {
                opacity:op?Animated.interpolate(timmingAnimationQuick, {
                    inputRange: [0, 1],
                    outputRange: [1, .3]}):.3}
        }else{
            return {transform:[{translateX:0}]}
        }

        
    }
    useCode(() => cond(eq(timming.current,0), set(timming.current, 1)), [])

    return(
        <TouchableOpacity onPress={responder}>
            <Animated.View style={getTrans()}>
                <Animated.View style={{...styles.btnStyle, borderColor:selected? right?'green':'red':'white'}}>
                    <Text style={{fontSize:15, margin:15}}>{resposta}</Text>
                </Animated.View>
            </Animated.View>
        </TouchableOpacity>
    )
}


const styles = StyleSheet.create({
    btnStyle:{
        backgroundColor:'white',
        borderWidth:2,
        borderRadius:35,
        borderColor:'white',
        minHeight:65,
        alignItems:'center',
        justifyContent:'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    }
})

export default BtnQuiz


{/*<Animated.View style={{opacity:!answered?Animated.interpolate(timmingAnimation, {
    inputRange: [0, .5, 1],
    outputRange: [0, 0, 1]}):1}}>
<Animated.View style={{...styles.btnStyle, borderColor:selected? right?'green':'red':'white'}}>
    <Text style={{fontSize:15, margin:15}}>{resposta}</Text>
</Animated.View>
</Animated.View>*/}