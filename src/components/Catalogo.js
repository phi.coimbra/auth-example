import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";

function Catalogo(props) {
  return (
    <View style={[styles.container, props.style]}>
      <View style={styles.rect2}>
        <View style={styles.group3ColumnRow}>
          <View style={styles.group3Column}>
            <View style={styles.group3}>
              <Text style={styles.text}>Catálogo</Text>
              <Text style={styles.diversasOpcoes}>diversas opções</Text>
            </View>
            <View style={styles.group2}>
              <View style={styles.icon2Row}>
                <FontAwesomeIcon
                  name="clock-o"
                  style={styles.icon2}
                ></FontAwesomeIcon>
                <Text style={styles.catalogo2}>19/01/2020</Text>
                <FontAwesomeIcon
                  name="download"
                  style={styles.icon3}
                ></FontAwesomeIcon>
                <Text style={styles.text2}>122</Text>
              </View>
            </View>
          </View>
          <View style={styles.group4}>
            <View style={styles.rect3}>
              <FontAwesomeIcon
                name="cloud-download"
                style={styles.icon}
              ></FontAwesomeIcon>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  rect2: {
    width: 313,
    height: 87,
    backgroundColor: "rgba(251,251,251,1)",
    elevation: 15,
    borderRadius: 6,
    shadowOffset: {
      height: 7,
      width: 0
    },
    shadowColor: "rgba(0,0,0,1)",
    shadowOpacity: 0.05,
    shadowRadius: 5
  },
  group3: {
    width: 120,
    height: 35
  },
  text: {
    color: "rgba(74,144,226,1)",
    fontSize: 16,
    fontFamily: "Montserrat-Medium",
    lineHeight: 17
  },
  diversasOpcoes: {
    color: "rgba(155,155,155,1)",
    fontSize: 10,
    fontFamily: "Montserrat-Medium",
    lineHeight: 17,
    marginTop: 1
  },
  group2: {
    width: 136,
    height: 17,
    flexDirection: "row",
    marginTop: 6
  },
  icon2: {
    color: "rgba(207,204,204,1)",
    fontSize: 15,
    marginTop: 1
  },
  catalogo2: {
    color: "rgba(155,155,155,1)",
    fontSize: 10,
    fontFamily: "Montserrat-Medium",
    lineHeight: 17,
    marginLeft: 6
  },
  icon3: {
    color: "rgba(207,204,204,1)",
    fontSize: 15,
    marginLeft: 29,
    marginTop: 1
  },
  text2: {
    color: "rgba(155,155,155,1)",
    fontSize: 10,
    fontFamily: "Montserrat-Medium",
    lineHeight: 17,
    marginLeft: 6
  },
  icon2Row: {
    height: 17,
    flexDirection: "row",
    flex: 1,
    marginRight: 1
  },
  group3Column: {
    width: 136,
    marginTop: 16,
    marginBottom: 13
  },
  group4: {
    width: 45,
    height: 87,
    marginLeft: 102
  },
  rect3: {
    width: 45,
    height: 87,
    backgroundColor: "rgba(126,211,33,1)",
    elevation: 15,
    borderRadius: 6,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    shadowOffset: {
      height: 7,
      width: 0
    },
    shadowColor: "rgba(0,0,0,1)",
    shadowOpacity: 0.05,
    shadowRadius: 5
  },
  icon: {
    color: "rgba(255,255,255,1)",
    fontSize: 20,
    height: 20,
    width: 21,
    marginTop: 34,
    marginLeft: 12
  },
  group3ColumnRow: {
    height: 87,
    flexDirection: "row",
    marginLeft: 30
  }
});

export default Catalogo;
