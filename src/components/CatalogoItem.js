import React, {useEffect, useState} from 'react';
import {View, StyleSheet, Image, Text, Dimensions} from 'react-native'
import { useFocusEffect } from '@react-navigation/native';
import MaterialButtonIn from "../components/MaterialButtonIn";

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

CatalogoItem = (props) => {
 
    const {imagem, titulo, botaotext, page, navigation, content, type} = props
    const [imageSize, setImageSize] = useState({width:0, height:0, btnname:botaotext})
    
    //const image = Image.resolveAssetSource(url)

    const {width, height} = Dimensions.get('window')

    useFocusEffect(
        React.useCallback(() => {
            setImageSize(prev => ({...prev, btnname:botaotext}))
            // Do something when the screen is focused
            return () => {
            // Do something when the screen is unfocused
            // Useful for cleanup functions
            //console.log( myvideo)
            //setPause(true)
          //  setOff(false)
            };
        }, []),
    );

    useEffect(() => {
        if(imageSize.btnname === 'aguarde'){
            navigation.navigate(page, { screenName: page, content:content })
        }
    }, [imageSize])

    openFile = () => {
        
    }

    return (
        <View style={[styles.container, {height:height/3}]}>
            <View style={{flex: 1, margin:15, justifyContent:'space-between'}}>
                <Image
                    source={imagem.nome}
                style={styles.image}
                //resizeMode='contain'
                />
                <View style={{marginTop:10, flexDirection:'row', justifyContent:'space-between'}}>
                    <Text style={styles.titulo}>{titulo}</Text> 
                    <Icon name={type === 'pdf'?'file-pdf-outline':'file-video-outline'} size={25}/>
                </View>
                <MaterialButtonIn
                    text1={imageSize.btnname}
                    onPress={() => {setImageSize(prev => ({...prev, btnname:'aguarde'}))}}
                    style={styles.materialButtonIn}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        marginTop:20,
        backgroundColor:'#fff',
        borderRadius:10
        
    },
    materialButtonIn:{
        alignItems: 'stretch',    
        marginTop:10,
        height: 50,
       // marginRight:10,
        //marginLeft:10
      },
      titulo:{
        color: "#000",
        fontSize: 17,
        fontFamily: "Montserrat-Regular",
        //marginTop:20,
       
        //lineHeight: 20

      },
      image: {
        flex:1,
        height: undefined, 
        width: undefined,
        alignSelf: 'stretch',
        borderRadius:10
   
      }
})

export default CatalogoItem