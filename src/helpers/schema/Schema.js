export const imageSchema = {
    version: 0,
    title: 'imagem schema',
    description: 'imagens dos perfis',
    type: 'object',
    properties: {
        email: {
            type: 'string',
        },
        id: {
            type: 'string',
            primary: true,
        },
        col:{
            type: 'string',
            default:'images'
        },
    },
}

export const eventoSchema = {
    version: 0,
    title: 'evento schema',
    description: 'nomes de eventos',
    type: 'object',
    properties: {
        nome: {
            type: 'string',
        },
        id: {
            type: 'string',
            primary: true,
        },
        col:{
            type: 'string',
            default:'evento'
        },
    }
}


export const userSchema = {
    version: 0,
    title: 'user schema',
    description: 'usuarios e cadastro de informações',
    type: 'object',
    properties: {
        id: {
            type: 'string',
            primary: true,
        },
        nome: {
            type: 'string',
        },
        sobrenome: {
            type: 'string',
        },
        email: {
            type: 'string',
        },
        logged: {
            type: 'boolean',
            default:false
        },
        evento:{
            type: 'string',
            default:''
        },
        device:{
            type: 'string',
            default:''
        },
        col:{
            type: 'string',
            default:'users'
        },
        image: {
            type: 'string',
            default:'http://192.168.1.48:3030/images/avatar.png'
        },
    },
    required: ['nome', 'sobrenome', 'email'],
};

export const quizSchema = {
    version: 0,
    title: 'schema de quiz',
    type: 'object',
    description: 'quiz para o evento',
    properties: {
        id: {
            type: 'string',
            primary: true,
        },
        info: {
            type: 'string'
        },
        evento: {
            type: 'string'
        },
        perguntas: {
            type: 'array',
            items: {
                type: 'object',
                properties: {
                    titulo: {
                        type: 'string'
                    },
                    alternativas: {
                        type: 'array',
                        items: {
                            type: 'object',
                            properties: {
                                titulo: {
                                    type: 'string'
                                }
                            }
                        }       
                    },
                    resposta: {
                        type: 'string'
                    }
                }
            }       
        }
    }
  };