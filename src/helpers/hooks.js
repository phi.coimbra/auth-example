import React, {useState, useEffect} from "react"
import { getUniqueId, getSystemName } from 'react-native-device-info'

export const DB = React.createContext('DB');
export const Dispatch = React.createContext('Dispatch');
export const Auth = React.createContext('Auth');
export const Users = React.createContext('Users');

export const Client = 'antidoto';
export const uniqueID = getUniqueId();



export function logginReducer(state, action) {
    console.log('reducer', state, action)
    switch(action.type){
        case 'login' : {
            return { 
                ...state,
                logged:true
            }
            
        }
        case 'logout' : {
            return {
                ...state,
                logged:false
            }
        }
        case 'cliente' : {
            return {
                ...state,
                cliente:action.payload
            }
        }
        case 'navigation' : {
            return {
                ...state,
                navigation:action.payload
            }
        }
        case 'navigate' : {
            state.navigation.navigate(action.payload, { screenName: action.payload})
            return {
                ...state,
                navigate:action.payload
            }
        }
        case 'userdata' : {
            return {
                ...state,
                user:action.payload
            }
        }
        default : {
            return state     
        } 
    }
}

export const findUser = (gun, {email, senha}) => new Promise((resolve, reject) =>  {

    const user = gun.user();
    console.log('myuser', email, senha)

    user.auth(email, senha, async ack => {
        // logged in!
        console.log('login:', 'pair', gun.user().pair().pub)
        if(ack.err){
            console.log('doesnt exist')
            resolve( false )
            //dispatch({type:'login'})
          //createUser(values)
        //}else if(!ack.pub){
          //  console.log('cant ')
            //resolve( false )
        }else{
            console.log('logged', uniqueID)
            //gun.get(Client).get('devices').get(uniqueID).get('logged').put({islogged:true})
            resolve( true )
        }
        
      });
      
})

export const getCliente = (db, email) => new Promise((resolve, reject) =>  {
    /*gun.get('devices').get(uniqueID).get('cliente').once(item => {
        if(item){
            console.log('tem cliente:', item.cliente)
            resolve(item.cliente)
        }else{
            console.log('no tem cliente:', item)
            resolve()
        }
    })*/
    db.users.findOne().where('email').eq(email).exec().then(doc => {
        console.log('getCliente: ', doc)
        if(doc){
            console.log('getCliente: ', doc.evento)
            resolve(doc.evento)
        }else{
            resolve(null)
        }
    });
    
})

export const setLoggedUser = (db, email) => new Promise((resolve, reject) =>  {
  //  gun.get('devices').get(uniqueID).get('cliente').put({cliente:cliente});

    db['users'].upsert({
        email:'phi.coimbra@gmail.com',
        logged:true
    });

    resolve()
})



/*export const createUser = (gun, values) => new Promise((resolve, reject) =>  {

    const user = gun.user();

    user.create(values.email, values.senha, function(ack){
      // done creating user!
      const info = {
        os:getSystemName()
      }
      console.log('done creating user!', ack.pub);
      //gun.get(Client).get('devices').get(uniqueID).get('logged').put({islogged:true});
     // gun.get(Client).get('devices').get(uniqueID).get('info').put(info);
      resolve( true );
    });
  })*/

export const isLogged = (db, client) => new Promise((resolve, reject) =>  {
    

  /*  gun.get(Client).get('devices').get(uniqueID).get('logged').once(logged => {
        console.log('logged?:', logged);
        if(logged){
            resolve( logged.logged );
        }else{
            
            resolve(false);
        }
    })*/

    //return false
    resolve( true );

})

export function logout(auth){
    auth.logout();
   // gun.user().leave();
   // gun.get(Client).get('devices').get(uniqueID).get('logged').put({islogged:false});
}

export const createAccount = (auth, {email, senha}) => new Promise((resolve, reject) =>  {
    const userData = {email:email, password:senha};
    return auth.service('users').create(userData).then((result) => {
      //return authenticate(Object.assign(userData, {strategy: 'local'}))
      console.log('trycreate', result)
      
        resolve(true)
    }).catch(err => {
        console.log('ja existe', err)
        resolve(false)
    });
  })

  



  //createAccount('phillipe@antidotodesign.com.br', 'virus');
  //login('phillipe@antidotodesign.com.br', 'virus');

 // messageService.on('created', message => console.log('Created a message', message));

// Use the messages service from the server
/*messageService.create({
  text: 'Message from client'
});*/

export const reAuth = (auth) => new Promise((resolve, reject) =>  {
    auth.reAuthenticate().then((e) => {
    // show application page
    console.log('LOGGED')
    resolve(e)
    }).catch(() => {
    // show login page
    console.log('NO TOKEN')
    resolve(false)
    });
})

export const login = (auth, {email, senha}, gun) => new Promise((resolve, reject) =>  {
    console.log('try', email, senha)
    auth.authenticate({
    strategy: 'local',
    email: email,
    password: senha
  }).then((e) => {
    // Logged in
   // gun.get('devices').get(uniqueID).get('dados').put({email:email});
    console.log('LOGGED')
    resolve(e)
  }).catch(e => {
    // Show login page (potentially with `e.message`)
    console.log('Authentication error', e);
    resolve(false)
  });
})

export const userData = (data) => {
    const [userdata, setUserdata] = useState(data);

    return [
        userdata,
        function(event) {
            console.log('event:', event)
            setUserdata(event);
        }
      ];
}