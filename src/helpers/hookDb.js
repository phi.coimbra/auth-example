import { useState, useEffect } from "react";

export default function useFetch(url) {
  const [data, setData] = useState('ala');

  useEffect(() => {
    setData('issotutto')
  }, []);

  return data;
}