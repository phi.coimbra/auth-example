/**
 * @format
 */
//45394fd0-0ad9-4438-be0e-70980c68d117 PUSH
//import '@gooddollar/gun-asyncstorage'
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';


console.disableYellowBox = true;

AppRegistry.registerComponent(appName, () => App);
